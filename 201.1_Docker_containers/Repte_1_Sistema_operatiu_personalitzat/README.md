#1) Dockerfile
Primero he creado el fichero Dockerfile

#2) Generar imagen
He generado la imagen personalizada a partir del dockerfile:
docker build -t javierfb/debian_repte1 .

#3) Pruebo la imagen generando un container a partir de esta:
docker run --rm -it javierfb/debian_repte1 /bin/bash

o directamente ejecutando algun comando concreto:

docker run --rm -it javierfb/debian_repte1 ip a

