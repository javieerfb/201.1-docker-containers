#! /bin/bash

sed -i "s\cn=Manager\cn=$ldap_rootdn\g" slapd.conf 
sed -i "s\rootpw secret\rootpw $ldap_rootpw\g" slapd.conf

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0 


