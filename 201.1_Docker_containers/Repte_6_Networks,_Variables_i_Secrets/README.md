# Enunciado

> Repte 6 Networks, Variables i Secrets
> Apunts: HowTo_ASIX_Docker.pdf capítol “Docker bàsic”.
> Implementar un servidor LDAP on el nom de l’administrador de la base de dades de l’escola sigui captat a través de variables d’entorn.
> Implementar un servidor LDAP on el nom de l’administrador de la base de dades de l’escola sigui captat a través de docker secrets.

# Desglose o como se diga
Se pueden pasar variables de entorno del SO al docker, poniendo la opción -e en el docker run.
Pueden ser: 
+ Variables ya existentes del entorno (deben ser exportadas con "export "nombre_variable" ")

Definidas en el propio docker run, es necesario poner -e "nombre_variable que ya he definido en el entorno del host"

+ Para definirla en la propia ejecución del docker run, ha que hacer -e "se definiría como cualquier otra variable --> nombre_variable=valor_variable"


## Variable de entorno
En el fichero slapd.conf se encuentra el nombre del root, y su contraseña
por lo tanto si cambio el nombre deseado por una variable string del mismo formato, debería funcionar:
Está substitución se ejecutará en el fichero starup al inicio, 2 comandas que substituirán ambas líneas

### El fichero slapd.conf resultante debe quedar tal que así:
```
# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Javier,dc=edt,dc=org"
rootpw javier
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Javier,dc=edt,dc=org" read
       by * none

```

### Pruebas:
En las pruebas esto ha servido:
sed "s\cn=Manager\cn=$ldap_rootdn\g" slapd.conf 
sed "s\rootpw secret\rootpw $ldap_rootpw\g" slapd.conf

En el startup le añado la opción -i para que se aplique la substitución


# Inicialización
## Para las variables:
Primero la defino y luego la exporto para que forme parte del entorno:
```
$ ldap_rootdn="Javier"
$ ldap_rootpw="javier"
$ export ldap_rootpw
$ export ldap_rootdn
```

## Iniciar el docker
$ docker run --rm --name ldap_variable -h ldap_variable -e ldap_rootpw -e ldap_rootdn -d javierfb/ldap23:variable

## Comprobar estado del fichero de configuración slapd.conf:
```
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_6_Networks,_Variables_i_Secrets$ docker exec -it ldap_variable cat slapd.conf | tail -15
# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Javier,dc=edt,dc=org"
rootpw javier
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Javier,dc=edt,dc=org" read
       by * none

```

## Comprobar credenciales
A continuación de identifico como el usuario ldap, en este caso root, en la base de datos ldap:
```
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_6_Networks,_Variables_i_Secrets$ docker exec -it ldap_variable /bin/bash
root@ldap_variable:/opt/docker# ldapwhoami -x -D 'cn=Javier,dc=edt,dc=org' -w javier
dn:cn=Javier,dc=edt,dc=org
root@ldap_variable:/opt/docker# ldapwhoami -x -D 'cn=Manager,dc=edt,dc=org' -w secret
ldap_bind: Invalid credentials (49)
root@ldap_variable:/opt/docker# exit
exit

```
