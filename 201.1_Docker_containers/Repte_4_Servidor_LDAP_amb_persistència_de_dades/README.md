# Repte 4 Servidor LDAP amb persistència de dades
> Apunts: HowTo_ASIX_Docker.pdf capítol “Docker bàsic”.
Implementar una imatge que actuï com a servidor ldap amb persistència de dades tant de configuració com les dades de la base de dades d’usuaris.
>

Empezaré haciendo la prueba con el container que menciona la práctica al final (en la anotación):
```
docker pull edtasixm06/ldap20:entrypoint



```

Según el README.md, esta es la descripción del container:


> edtasixm06/ldap20:entrypoint Imatge amb varies opcions d'arrencada segons el valor que passem: start, initdb, initdbedt. Usem volumes per tenir permanència de dades.

    initdbedt crea tota la base de dades edt (esborra tot el que hi havia prèviament). Hi posa la xixa.
    initdb esborra tot el que hi havia i crea la base de dades sense xixa.
    start engega el servidor
>

Lo enciendo:
```
docker run --rm -it --name ldap.edt.org -d edtasixm06/ldap20:entrypoint initdbedt

```

Comrpueba que los datos están bien:

```
$ docker exec -it ldap.edt.org ldapsearch -x -LLL -H ldap://localhost -b 'dc=edt,dc=org' | head
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux

$ docker exec -it ldap.edt.org ldapsearch -x -LLL -H ldap://localhost -b 'dc=edt,dc=org' | wc -l
354

```

Ahora con tal de aprender y para comprobar que AÚN no persisten los datos, practico un poco ldapdelete:
```
$ docker exec -it ldap.edt.org ldapdelete -x -D 'cn=Manager,dc=edt,dc=org' -H ldap://localhost -w secret cn=alumnes,ou=grups,dc=edt,dc=org

$ docker exec -it ldap.edt.org ldapdelete -x -D 'cn=Manager,dc=edt,dc=org' -H ldap://localhost -w secret uid=user09,ou=usuaris,dc=edt,dc=org uid=user08,ou=usuaris,dc=edt,dc=org
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_4_Servidor_LDAP_amb_persistència_de_da

$ docker exec -it ldap.edt.org ldapsearch -x -LLL -H ldap://localhost -b 'dc=edt,dc=org' | wc -l
313

```

Ahora salgo, y vuelvo a hacer un ldapsearch, se puede observar que no se ha guardado absolutamente nada de lo que he hecho :(

```
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_4_Servidor_LDAP_amb_persistència_de_dades$ docker stop ldap.edt.org
ldap.edt.org
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_4_Servidor_LDAP_amb_persistència_de_dades$ docker run --rm -it --name ldap.edt.org -d edtasixm06/ldap20:entrypoint
5bdf890feae9deb15dcaaf69dd69944b949ec5029ee379d15f9df833b68653a9
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_4_Servidor_LDAP_amb_persistència_de_dades$ docker exec -it ldap.edt.org ldapsearch -x -LLL -H ldap://localhost -b 'dc=edt,dc=org' | wc -l
1

```

Creo los volumenes y al hacer la inicialización del container los asigno a las 2 ubicaciones donde se guarda la configuración y los datos de la BD respectivamente:
```
$ docker volume create ldap.datos
$ docker volume create ldap.configuracion
```

Apago el container y lo vuelvo a iniciar con una nueva BD, elimino cosillas, y lo apago otra vez.
```
$ docker stop ldap.edt.org

$ docker run --rm -it --name ldap.edt.org -v ldap.configuracion:/etc/openldap/slapd.d -v ldap.datos:/var/lib/ldap -d edtasixm06/ldap20:entrypoint initdbedt

$ docker exec -it ldap.edt.org ldapdelete -x -D 'cn=Manager,dc=edt,dc=org' -H ldap://localhost -w secret uid=user09,ou=usuaris,dc=edt,dc=org uid=user08,ou=usuaris,dc=edt,dc=org

$ docker stop ldap.edt.org
```

Aquí la inicialización con los volumenes (No hace ni falta ponerle opción, se habrá guardado todo):

```
$ docker run --rm -it --name ldap.edt.org -v ldap.configuracion:/etc/openldap/slapd.d -v ldap.datos:/var/lib/ldap -d edtasixm06/ldap20:entrypoint

$ docker exec -it ldap.edt.org ldapsearch -x -LLL -H ldap://localhost -b 'dc=edt,dc=org' | wc -l
322
```
