# Enunciado
> Apunts: HowTo_ASIX_Docker.pdf capítol “Docker compose”.
> Practicar les diverses subcomandes de docker-compose i observar el problema que es presenta quan es realitzen rèpliques de serveis amb ports fixes.
> Implementar l’exemple de rèpliques utilitzant ports dinàmics.

Los apuntes:
gitlab.com/edtasixm05/docker/3_docker_intermig

No puedo tener muchas máquinas todas conectadas al mismo puerto, con la tecnologia actual, si con el swarm, normalmente se usa Qubernetes, pero swarm es mas facil.

# Empezar
/tmp$ git clone https://gitlab.com/edtasixm05/docker.git
$ cd docker/3_docker_intermig/compose/subordres/

Una vez en el fichero docker-compose.yml,
eliminamos los servicios portainer y visualizer


La aplicación que lanzará el docker compose tendrá el nombre del directorio de contexto, en mi caso app
a201037jf@i06:/tmp/docker/3_docker_intermig/compose/subordres$ mkdir app
a201037jf@i06:/tmp/docker/3_docker_intermig/compose/subordres$ cp compose.yml app/
a201037jf@i06:/tmp/docker/3_docker_intermig/compose/subordres$ cd app/
a201037jf@i06:/tmp/docker/3_docker_intermig/compose/subordres/app$ vim compose.yml

## Teoria del docker compose
+ **docker compose top muestra los procesos de cada servicio**
(!) Para que muestre 1 servicio en concreto, hay que especificar el nombre del servicio tal y como se llama en el fichero compose.yml

### docker compose logs
```
$ docker compose logs redis 
$ docker compose logs
```

### docker compose port
```
$ docker compose port web 80
0.0.0.0:80
$ docker ps
CONTAINER ID   IMAGE                             COMMAND                  CREATED         STATUS         PORTS                                       NAMES
7797f8bca3f6   edtasixm05/getstarted:comptador   "python app.py"          6 minutes ago   Up 6 minutes   0.0.0.0:80->80/tcp, :::80->80/tcp

```

0.0.0.0 por todos los agujeros 80 que tenga mi ordenador
Cada interficie tiene 1 puerto 80

### docker compose start/stop
Se puede iniciar/parar un container individualmente

```
$ docker compose stop redis
[+] Stopping 1/1
 ✔ Container app-redis-1  Stopped                                                                0.3s 
$ docker compose start redis
[+] Running 1/1
 ✔ Container app-redis-1  Started    
```


## Reanudación de la práctica
```
a201037jf@i06:/tmp/docker/3_docker_intermig/compose/subordres/app$ docker compose up -d
[+] Building 0.0s (0/0)                                                                               
[+] Running 3/3
 ✔ Network app_webnet     Created                                                                0.1s 
 ✔ Container app-redis-1  Started                                                                0.6s 
 ✔ Container app-web-1    Started                                                                0.6s 
a201037jf@i06:/tmp/docker/3_docker_intermig/compose/subordres/app$ docker compose ps
NAME                IMAGE                             COMMAND                  SERVICE             CREATED             STATUS              PORTS
app-redis-1         redis                             "docker-entrypoint.s…"   redis               3 seconds ago       Up 2 seconds        0.0.0.0:6379->6379/tcp, :::6379->6379/tcp
app-web-1           edtasixm05/getstarted:comptador   "python app.py"          web                 3 seconds ago       Up 2 seconds        0.0.0.0:80->80/tcp, :::80->80/tcp
a201037jf@i06:/tmp/docker/3_docker_intermig/compose/subordres/app$ docker compose top
app-redis-1
UID   PID    PPID   C    STIME   TTY   TIME       CMD
999   9444   9381   0    12:34   ?     00:00:00   redis-server *:6379   

app-web-1
UID    PID    PPID   C    STIME   TTY   TIME       CMD
root   9452   9424   0    12:34   ?     00:00:00   python app.py   

a201037jf@i06:/tmp/docker/3_docker_intermig/compose/subordres/app$ docker compose top redis
app-redis-1
UID   PID    PPID   C    STIME   TTY   TIME       CMD

```

### Problematica de 2 o mas demonios escuchando por el mismo puerto
Intentamos iniciar web1, 2 y 3, pero solo consigue iniciar el 1 porque solo puede haber 1 escuchando por el m¡puerto 80

```
$ docker compose up --scale web=3
[+] Building 0.0s (0/0)                                                                               
[+] Running 4/4
 ✔ Container app-redis-1  Running                                                                0.0s 
 ✔ Container app-web-3    Created                                                                0.1s 
 ✔ Container app-web-1    Recreated                                                             10.4s 
 ✔ Container app-web-2    Created                                                                0.1s 
Attaching to app-redis-1, app-web-1, app-web-2, app-web-3
app-web-1    |  * Serving Flask app "app" (lazy loading)
app-web-1    |  * Environment: production
app-web-1    |    WARNING: This is a development server. Do not use it in a production deployment.
app-web-1    |    Use a production WSGI server instead.
app-web-1    |  * Debug mode: off
app-web-1    |  * Running on http://0.0.0.0:80/ (Press CTRL+C to quit)
Error response from daemon: driver failed programming external connectivity on endpoint app-web-2 (b2b909b5e605a1300f0220782117e815dc93dd81484386eda1ed1d78b837ea53): Bind for 0.0.0.0:80 failed: port is already allocated


$ docker compose ps
NAME                IMAGE                             COMMAND                  SERVICE             CREATED              STATUS              PORTS
app-redis-1         redis                             "docker-entrypoint.s…"   redis               11 minutes ago       Up 3 minutes        0.0.0.0:6379->6379/tcp, :::6379->6379/tcp
app-web-1           edtasixm05/getstarted:comptador   "python app.py"          web                 About a minute ago   Up About a minute   0.0.0.0:80->80/tcp, :::80->80/tcp
```

### Replicas de servicios con puertos dinámicos

1. Quitamos la propagación del servicio web
2. Ahora volvemos a probar a iniciar 3 webs
```
$ docker compose up -d --scale web=3
[+] Building 0.0s (0/0)                                                                               
[+] Running 4/4
 ✔ Container app-redis-1  Started                                                                0.6s 
 ✔ Container app-web-2    Started                                                                0.6s 
 ✔ Container app-web-1    Started                                                                0.9s 
 ✔ Container app-web-3    Started                                                                1.2s
```

3. en lugar de la -p, mejor el -P, ya que propaga el puerto que le da la gana (a partir de los disponibles (32000 y pico))
```
$ docker compose ps
NAME                IMAGE                             COMMAND                  SERVICE             CREATED             STATUS              PORTS
app-web-1           edtasixm05/getstarted:comptador   "python app.py"          web                 3 minutes ago       Up 2 minutes        0.0.0.0:32772->80/tcp, :::32772->80/tcp
app-web-2           edtasixm05/getstarted:comptador   "python app.py"          web                 3 minutes ago       Up 2 minutes        0.0.0.0:32771->80/tcp, :::32771->80/tcp
app-web-3           edtasixm05/getstarted:comptador   "python app.py"          web                 3 minutes ago       Up 2 minutes        0.0.0.0:32773->80/tcp, :::32773->80/tcp

```

4. Se puede comprobar en el navegador como siempre
5. El problema es que no tienen el mismo puerto, para solucionar el problema hay que ir a docker swarm y demás.

# SWARM (enjambre)
Ahora en lugar de desplegar las aplicaciones con docker compose, ahora se llamará **stack**.
Esto significa que el stack simplemente hace lo mismo pero crea la aplicación en un docker swarm
