# Docker Postgres23 (Javier Fons)
## Como funciona
+ Iniciarlo en detach:
```
$ docker run --rm --name training -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -d javierfb/postgres23
```

+ Comprobar la BBDD training por ejemplo:
```
$ docker exec -it training psql -U postgres -d training

```

+ Una vez dentro ya se pueden hacer **cositas**:
```
psql (16.0 (Debian 16.0-1.pgdg120+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(5 rows)

training=# \d repventas
                       Table "public.repventas"
   Column    |         Type          | Collation | Nullable | Default 
-------------+-----------------------+-----------+----------+---------
 num_empl    | smallint              |           | not null | 
 nombre      | character varying(15) |           | not null | 
 edad        | smallint              |           |          | 
 oficina_rep | smallint              |           |          | 
 titulo      | character varying(10) |           |          | 
 contrato    | date                  |           | not null | 
 director    | smallint              |           |          | 
 cuota       | numeric(8,2)          |           |          | 
 ventas      | numeric(8,2)          |           | not null | 
Indexes:
    "repventas_pkey" PRIMARY KEY, btree (num_empl)
```
