# postgres y adminer
javierfb/postgres23
adminer (container oficial)

Iniciar los containers:
```
$ docker compose -f docker-compose-adminer-postgres.yml up -d
[+] Running 3/3
 ✔ Network postgresyadminer_2hisx  Created                                                0.1s 
 ✔ Container adminer               Started                                                0.2s 
 ✔ Container training              Started                                                0.1s 
```

Comprobar (están los 2 funcionando):
```
javierf@debian:/var/tmp/201.1-docker-containers/Repte_8_Docker_compose:_iniciació/postgresYadmi
ner$ docker compose -f docker-compose-adminer-postgres.yml ps
NAME       IMAGE                 COMMAND                                             SERVICE    CREATED          STATUS          PORTS
adminer    adminer               "entrypoint.sh php -S [::]:8080 -t /var/www/html"   adminer    18 seconds ago   Up 17 seconds   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp
training   javierfb/postgres23   "docker-entrypoint.sh postgres"                     postgres   18 seconds ago   Up 16 seconds   0.0.0.0:5432->5432/tcp, :::5432->5432/tcp
javierf@debian:/var/tmp/201.1-docker-containers/Repte_8_Docker_compose:_iniciació/postgresYadmi
ner$ docker compose -f docker-compose-adminer-postgres.yml top
adminer
UID        PID     PPID    C    STIME   TTY   TIME       CMD
systemd+   11196   11148   0    01:43   ?     00:00:00   php -S [::]:8080 -t /var/www/html   

training
UID        PID     PPID    C    STIME   TTY   TIME       CMD
systemd+   11203   11171   0    01:43   ?     00:00:00   postgres                                 
systemd+   11326   11203   0    01:43   ?     00:00:00   postgres: checkpointer                   
systemd+   11327   11203   0    01:43   ?     00:00:00   postgres: background writer              
systemd+   11329   11203   0    01:43   ?     00:00:00   postgres: walwriter                      
systemd+   11330   11203   0    01:43   ?     00:00:00   postgres: autovacuum launcher            
systemd+   11331   11203   0    01:43   ?     00:00:00   postgres: logical replication launcher   
systemd+   11373   11203   50   01:43   ?     00:00:00   postgres: autovacuum worker template1    

```

## Conectarme:
En el navegador: localhost:8080

Las credenciales:
**System:** PostgreSQL
**Server:** db
**Username:** postgres
**Password:** passwd
**Database:** training
