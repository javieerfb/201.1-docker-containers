# Docker Compose
El docker compose es como un nivel superior de orden.
Nos permite agrupar en 1 solo archivo el despliege de diversos containers, redes, volumenes y esas cosas.
Este fichero se suele llamar **compose.yml** o **docker-compose.yml**.
Si se llama de otro modo al ejecutar el comando habrá que especificar el fichero con "-f nombre_del_fichero".

Sigue un formato concreto por ser de tipo YAML.
El formato es el siguiente:

```
version: "2"
services:
    web:
        image: edtasixm05/web21:base
        container_name: web.edt.org
        hostname: web.edt.org
        ports:
            - "80:80"
        networks:
            - mynet
    portainer:
        image: portainer/portainer
        ports:
            - "9000:9000"
        volumes:
            - "/var/run/docker.sock:/var/run/docker.sock"
        networks:
            - mynet
networks:
    mynet:
```

## Prueba1 con portainer
portainer es un container prefabricado (servicio) que me permite administrar desde un entorno web gráfico todo lo del docker.
Hay que:
    Poner en el navegador:    localhost:9000
    Definir las credenciales:
        ● Definir el password de admin.
        ● Clicar create user.
        ● Seleccionar Local (Manage the Local Docker Environment)
        ● Clicar a connect.


1. Una vez he creado el fichero docker-compose.yml (lo he copiado tal cual de los apuntes)
2. Me conecto con:
**docker-compose up -d**

3. Explico un poco:
docker-compose options:
    + up encender
    + down apagar
    + Y muchos más que no me interesan aún
La -d es para detach, y no le especifico el documento porqué le he puesto 1 de los 2 nombres que usa por defecto

```
$ docker-compose up -d

WARNING: The Docker Engine you're using is running in swarm mode.

Compose does not use swarm mode to deploy services to multiple nodes in a swarm. All containers will be scheduled on the current node.

To deploy your application across the swarm, use `docker stack deploy`.

Creating network "ejemplo_portainer_mynet" with the default driver
Pulling web (edtasixm05/web21:base)...
base: Pulling from edtasixm05/web21
b93b55b43f66: Pull complete
3f525e109842: Pull complete
d77e694ba21e: Pull complete
ea976f502acc: Pull complete
e78d14ae9c7f: Pull complete
Digest: sha256:0a731cc24c61945cdc5cafc6996a5e7b143f06de43dfa371f13217b63d61423b
Status: Downloaded newer image for edtasixm05/web21:base
Pulling portainer (portainer/portainer:)...
latest: Pulling from portainer/portainer
772227786281: Pull complete
96fd13befc87: Pull complete
0bad1d247b5b: Pull complete
b5d1b01b1d39: Pull complete
Digest: sha256:47b064434edf437badf7337e516e07f64477485c8ecc663ddabbe824b20c672d
Status: Downloaded newer image for portainer/portainer:latest
Creating web.edt.org                   ... done
Creating ejemplo_portainer_portainer_1 ... done

```

## Ejemplo 2
Es literalmente el mismo fichero que el anterior, solo cambia web21 por net21 todo el rato.
y también los puertos, aquí son 3 puertos que hay que propagar.

```
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_8_Docker_compose:_iniciació$ docker compose -f docker-compose_netYportainer.yml up -d
[+] Running 4/4
 ✔ web 3 layers [⣿⣿⣿]      0B/0B      Pulled                                                    8.8s 
   ✔ 955615a668ce Pull complete                                                                 4.5s 
   ✔ 444ad065d063 Pull complete                                                                 7.1s 
   ✔ 296e8f25b140 Pull complete                                                                 7.1s 
[+] Building 0.0s (0/0)                                                                              
[+] Running 3/3
 ✔ Network repte_8_docker_compose_iniciaci_mynet          Created                               0.1s 
 ✔ Container repte_8_docker_compose_iniciaci-portainer-1  Started                               0.7s 
 ✔ Container net.edt.org                                  Started                               0.8s 

a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_8_Docker_compose:_iniciació$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS          PORTS                                                                                                 NAMES
82240fbd9263   portainer/portainer     "/portainer"             12 seconds ago   Up 11 seconds   8000/tcp, 9443/tcp, 0.0.0.0:9000->9000/tcp, :::9000->9000/tcp                                         repte_8_docker_compose_iniciaci-portainer-1
fa86254f8579   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   12 seconds ago   Up 11 seconds   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:19->19/tcp, :::19->19/tcp   net.edt.org

```

### Otra forma de revisar sus estados:
(!) Sin especificar el fichero con -f, las opciones de docker-compose no funcionan

```
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_8_Docker_compose:_iniciació$ docker-compose -f docker-compose_webYportainer.yml top
net.edt.org
UID     PID    PPID    C   STIME   TTY     TIME                CMD            
------------------------------------------------------------------------------
root   18935   18909   0   12:50   ?     00:00:00   /usr/sbin/xinetd -dontfork

repte_8_docker_compose_iniciaci-portainer-1
UID     PID    PPID    C   STIME   TTY     TIME        CMD    
--------------------------------------------------------------
root   18894   18807   0   12:50   ?     00:00:00   /portainer
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_8_Docker_compose:_iniciació$ docker-compose -f docker-compose_webYportainer.yml ps
                  Name                               Command             State                     Ports                  
--------------------------------------------------------------------------------------------------------------------------
net.edt.org                                 /usr/sbin/xinetd -dontfork   Up      0.0.0.0:13->13/tcp,:::13->13/tcp,        
                                                                                 0.0.0.0:19->19/tcp,:::19->19/tcp,        
                                                                                 0.0.0.0:7->7/tcp,:::7->7/tcp             
repte_8_docker_compose_iniciaci-            /portainer                   Up      8000/tcp,                                
portainer-1                                                                      0.0.0.0:9000->9000/tcp,:::9000->9000/tcp,
                                                                                 9443/tcp      
```

## Ejemplo ldap + phpldapadmin
Para acceder al phpldapadmin
localhost/phpldapadmin

Editar el fichero config la línea 310 mas o menos


