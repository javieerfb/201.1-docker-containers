#! /bin/bash

cp /opt/docker/phpldapadmin.conf /etc/httpd/conf.d/phpldapadmin.conf
cp /opt/docker/config.php  /etc/phpldapadmin/config.php
#cp /opt/docker/httpd.conf /etc/httpd/conf/httpd.conf	#Esto para el puerto por defecto del phpldapadmin

# Servicio ssh, pero versión difícil porqué no he iniciado con systemd con PID y las claves de host no se generan bien, por lo que hay que hacer todo manual...
dnf install openssh-clients openssh-server nmap -y

## genero las claves de host que no se han autogenerado por algo que desconozco
ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ""
ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key -N ""
ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ""
chmod 600 /etc/ssh/ssh_host_*

## Inicio el servicio ssh, lo que abre el puerto 22 :)
/usr/sbin/sshd


/sbin/php-fpm
/usr/sbin/httpd -D FOREGROUND

