# Docker compose de ldap y phpldapadmin

## docker-compose
El fichero es: docker-compose_ldapYphpldapadmin.yml

Su contenido:
 + javierfb/ldap23:editat
  + Puerto 80
 + javierfb/phpldapadmin:base
  + Puerto 389

## Ejecución:
```
$ docker compose -f docker-compose_ldapYphpldapadmin.yml up -d
[+] Building 0.0s (0/0)                                                                                                                                                                                                                                                          
[+] Running 2/2
 ✔ Container ldap.edt.org          Started                                                                                                                                                                                                                                  0.5s 
 ✔ Container phpldapadmin.edt.org  Started                                                                                                                                                                                                                                  0.5s 

```

## Verificación:
```
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_8_Docker_compose:_iniciació/ldapYphpldapadmin$ docker compose -f docker-compose_ldapYphpldapadmin.yml ps
NAME                   IMAGE                          COMMAND                  SERVICE             CREATED             STATUS              PORTS
ldap.edt.org           javierfb/ldap23:editat         "/bin/sh -c /opt/doc…"   ldap                7 seconds ago       Up 6 seconds        0.0.0.0:389->389/tcp, :::389->389/tcp
phpldapadmin.edt.org   javierfb/phpldapadmin23:base   "/bin/sh -c /opt/doc…"   phpldapadmin        7 seconds ago       Up 6 seconds        0.0.0.0:80->80/tcp, :::80->80/tcp
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_8_Docker_compose:_iniciació/ldapYphpldapadmin$ docker compose -f docker-compose_ldapYphpldapadmin.yml top
ldap.edt.org
UID    PID     PPID    C    STIME   TTY   TIME       CMD
root   28013   27983   0    13:00   ?     00:00:00   /bin/sh -c /opt/docker/startup.sh   
root   28080   28013   0    13:00   ?     00:00:00   /bin/bash /opt/docker/startup.sh    
root   28099   28080   0    13:00   ?     00:00:00   /usr/sbin/slapd -d0                 

phpldapadmin.edt.org
UID    PID     PPID    C    STIME   TTY   TIME       CMD
root   28006   27956   0    13:00   ?     00:00:00   /bin/bash /opt/docker/startup.sh              
root   28089   28006   0    13:00   ?     00:00:00   php-fpm: master process (/etc/php-fpm.conf)   
48     28090   28089   0    13:00   ?     00:00:00   php-fpm: pool www                             
48     28091   28089   0    13:00   ?     00:00:00   php-fpm: pool www                             
48     28092   28089   0    13:00   ?     00:00:00   php-fpm: pool www                             
48     28093   28089   0    13:00   ?     00:00:00   php-fpm: pool www                             
48     28094   28089   0    13:00   ?     00:00:00   php-fpm: pool www                             
root   28095   28006   0    13:00   ?     00:00:00   /usr/sbin/httpd -D FOREGROUND                 
48     28096   28095   0    13:00   ?     00:00:00   /usr/sbin/httpd -D FOREGROUND                 
48     28098   28095   0    13:00   ?     00:00:00   /usr/sbin/httpd -D FOREGROUND                 
48     28100   28095   0    13:00   ?     00:00:00   /usr/sbin/httpd -D FOREGROUND                 
48     28110   28095   0    13:00   ?     00:00:00   /usr/sbin/httpd -D FOREGROUND    
```

## Resultado Web
Para gestionar el ldap a través de PHPLDAPADMIN:
En el navegador escribir: **localhost/phpldapadmin**


