# ENUNCIADO


> Repte 3 Utilització de volums en un servidor Postgres
Apunts: HowTo_ASIX_Docker.pdf capítol “Docker bàsic”.
Usar postgres (la nostra versió) i una base de dades persistent. Usar la imatge
postgres original amb populate i persistència de dades. Exemples de SQL injectat
usant volumes.
>

## Postgres versión edtasixm05 y persisnetncia
1. Primero creo el volumen:
```
$ docker volume create mypostgresbd
mypostgresbd

Y vemos donde se guardarán los datos:

$ docker volume inspect mypostgresbd | grep Mount
        "Mountpoint": "/var/lib/docker/volumes/mypostgresbd/_data",

```

2. Luego iniciamos la imagen personalizada proporcionada en los apuntes, que cuenta con el servidor de postgres, y la base de datos training para poder practicar.

La imagen es esta: 
edtasixm05/getstarted:postgres


La forma de iniciarla (siendo /var/lib/docker/volumes/mypostgresbd/_data el volumen donde se almacenaran los cambios en el host) es:
```
$ docker run --rm --name training -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v mypostresdb:/var/lib/postgresql/data -d edtasixm05/getstarted:postgres

# Siendo "mypostgresbd" el volumen creado, y /var/lib/... la ubicación del container donde habŕá persistencia de datos.

$ docker ps
CONTAINER ID   IMAGE                            COMMAND                  CREATED         STATUS         PORTS      NAMES
9c1c79ad98d0   edtasixm05/getstarted:postgres   "docker-entrypoint.s…"   6 seconds ago   Up 5 seconds   5432/tcp   training

```

Para observar que los datos del postgres se han almacenado en el volumen hacemos:

```
$ docker exec -it training /bin/bash

root@5b11252f2ce1:/# du -hs /var/lib/postgresql/data/
51M	/var/lib/postgresql/data/

root@5b11252f2ce1:/# ls -l /var/lib/postgresql/data/
total 128
drwx------ 6 postgres postgres  4096 Sep 28 11:04 base
drwx------ 2 postgres postgres  4096 Sep 28 11:05 global
drwx------ 2 postgres postgres  4096 Sep 28 11:04 pg_commit_ts
drwx------ 2 postgres postgres  4096 Sep 28 11:04 pg_dynshmem


```


3. Hacemos cambios para comprobar luego que al apagar la máquina, se seguirán manteniendo:

```
root@68b45ce01dae:/# su - postgres
postgres@68b45ce01dae:~$ psql training
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(5 rows)
training=# DROP TABLE productos;
DROP TABLE
training=# CREATE table javier (
training(# nombre varchar(30)
training(# );
CREATE TABLE
training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | javier    | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | repventas | table | postgres
(5 rows)
```

4. Ahora se para el container, para volver a encenderlo y comprobar que se mantienen los cambios hechos en la base de datos:

```
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_3_Utilització_de_volums_en_un_servidor_Postg
res$ docker stop training
training
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_3_Utilització_de_volums_en_un_servidor_Postg
res$ docker run --rm --name training -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v mypostgresbd:/var/lib/postgresql/data -d edtasixm05/getstarted:postgres
be733270271e6378c8fb3b499e7b4337e5bbabc20f5b9a01d6ed023e006050a5
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_3_Utilització_de_volums_en_un_servidor_Postg
res$ docker exec -it training psql -d training -U postgres
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | javier    | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | repventas | table | postgres
(5 rows)

```

## Postgres original con populate y persistencia
1. Crear el volumen

2. Usar un directorio con datos y scripts (de los apuntes git) para hacer el populate, y definirlo como directorio activo en el container al iniciarlo
git clone https://gitlab.com/edtasixm05/docker.git

La ruta será esta:
cd docker/2_docker_basic/mounts/postgres/training/

3. Iniciar la máquina (la primera -v es para hacer un bind mount del directorio activo del host en el directorio preparado por postgres)
(la segunda -v es para el volumen creado donde habrá la persistencia)
```
$ docker run --rm --name training -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd):/docker-entrypoint-initdb.d -v datos_postgres_original:/var/lib/postgresql/data -d postgres

docker exec -it training psql -d training -U postgres
```

## SQL injectado usando volumenes

Pág 78 - 80  de HowTo-ASIX-Docker



## Corrección con el Canet
```
$ docker network create mynet
2ad311271358157c330df6389fc15e73e25c7e7aae69d0e4bbb1c92b9912cb65
$ docker run --rm --name training -e POSTGRES_PASSWORD=passwd --net mynet -d postgres
$ docker ps
CONTAINER ID   IMAGE      COMMAND                  CREATED         STATUS         PORTS      NAMES
154e4d2c90ab   postgres   "docker-entrypoint.s…"   4 minutes ago   Up 4 minutes   5432/tcp   training
$ docker exec -it training /bin/bash
```

Inspeccionar los logs del docker iniciado: docker logs training


#El cliente:
```
$ docker run --rm --name psql --net mynet -it postgres /bin/bash
Se puede ver el direcriorio de datos.

Me conecto al otro contenedor:
(Compartimos misma red (mynet))

#psql -h 172.18.0.2 -U postgres -d postgres
Password for user postgres: 
psql (16.0 (Debian 16.0-1.pgdg120+1))

Iniciar sesión interactiva directamente en psql desde el cliente psql (el container de nombre psql)
$ docker run --rm --name psql --net mynet -it postgres psql -h 172.18.0.2 -U postgres -d postgres

Iniciar directamente en el servidor (ya iniciado)
No hace falta especificar el host porque es en local.
$ docker exec -it training psql  -U postgres -d postgres
```
### Otra prueba ahora con volumenes (Todo lo del directorio actual a un directorio especifico que ejecuta todo lo que tiene dentro)

docker run --rm --name training -h bd.edt.org -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd):/docker-entrypoint-initdb.d --net mynet -d postgres

$ docker run --rm --name psql --net mynet -it postgres /bin/bash


Los volumenes no son mas que directorios en /var/lib/docker/volumes/<nombre_volumen>/_datadonde se guardan los datos.
