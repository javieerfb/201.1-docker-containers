# Primero creo el Dockerfile
Será necesario instalar tanto el servidor apache como algunos comandos básicos para comprobar su funcionamiento.
Además deberemos:
    exponer el puerto 80 (apache)
    iniciar el servicio apache en primer plano 
    


Para que el servidor esté en detach, el servicio como tal debe estar en primer plano funcionando siempre, de lo contrario, el container morirá porque ha finalizado el servicio por el cuál se había creado.
RESUMEN: Docker solo funciona cuando el servicio principal está vivo.

# Para iniciar el apache
```
Cal mirar el man de cada servei i identificar quin és l’executable.
Generalment en debian podem usar /etc/init.d/nomservei start [opció-foreground].
Podem mirar en el paquet del servei i mostrar el contingut del fitxer
/etc/systemd/system/nomservei.service i identificar quin és l’executable. Llavors
consultar el man per saber l’opció per enegegar-lo en foreground.
```

Como se ve en el manual apache2(8), dice textualmente que con -X el servidor no se despegará de la consola.
Sin embargo solo me ha funcionado con el apache2ctl (se ve citado en la sección SEE MORE del manual de apache2) para que de verdad se ponga en primer plano.

## Para comprobar que funciona el apache del docker
Solo es necesario hacer un wget de su ip (172.16.0.2), y visualizar el index.html obtenido.
También se puede usar el navegador, poniendo la IP del docker.
!Importante asegurarse de que el apache del host no está encendido.

# Detach y migración de puerto
Todo en la ejecución del container personalizado:

Para hacer el docker en modo detach es tan simple como quitar la opción -it y poner -d.
En la migración solo poner -p puerto_host:puerto_container

## Orden ejecución container en detach con migración de puerto
Bastará con hacer esta orden:   
docker run --rm -d -p 80:80 javieerfb/repte2

# Comprobación final

## Detach

Una vez ejecutada la orden para iniciar el container, haremos "docker ps" y podremos coprobar que el container sigue en ejecución, además de poder ver que su comanda en ejecución es la de apache2ctl.

Muestra "docker ps":
    CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS                               NAMES
8edaaaea1062   javieerfb/repte2   "/bin/sh -c 'apache2…"   9 minutes ago   Up 9 minutes   0.0.0.0:80->80/tcp, :::80->80/tcp   competent_moser


## Migración de puerto

1. Primero se comprueba que no tenemos abierto el puerto 80 en el host:
```
guest@i04:/var/tmp/201.1-docker-containers/Repte_2_Servidor_detach_amb_propagació_de_port
$ nmap localhost
Starting Nmap 7.93 ( https://nmap.org ) at 2023-09-27 12:48 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000063s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 996 closed tcp ports (conn-refused)
PORT    STATE SERVICE
22/tcp  open  ssh
25/tcp  open  smtp
111/tcp open  rpcbind
631/tcp open  ipp

Nmap done: 1 IP address (1 host up) scanned in 0.03 seconds
```

2. Al iniciar el container se vuelve a comprobar (ahora si esta abierto):

3. Se comprueba en el navegador mismo que poniendo mi ip de host (10.200.243.204)
se abre la página por defecto del apache.


