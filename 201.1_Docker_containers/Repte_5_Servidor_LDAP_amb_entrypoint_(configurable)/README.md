# ENUNCIADO:

> Apunts: HowTo_ASIX_Docker.pdf capítol “Docker bàsic”.
Implementar una imatge de servidor ldap configurable segons el paràmetre rebut.
S’executa amb un entrypoint que és un script que actuarà segons li indiqui
l’argument rebut:
● crear un servidor ldap buit, sense dades d’usuari.
● crear un servidor ldap inicialitzat amb les dades de l’organització de l’escola
(dades inicials).
● engegar el servei ldap utilitzant les dades que perdurin d’execucions
anteriors.
● mostrar amb slapcat la base de dades segons sigui el següent argument (0, 1
o res).


Aclaración:
Repte-5:

    canviar al Dockerfile el CMD per ENTRYPOINT

    executa l’script startup, però rep els arguments:

        initdb → ho inicialitza tot de nou i fa el populate de edt.org

        slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades

        start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents

        slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada 


>


# TEORIA:

CMD:
es la orden que ejecutará el container como proceso principal, se puede especificar al final del docker run o en el propio Dockerfile.
Siempre tendrá prioridad la orden que se indique al final, en el Dockerfile solo es la por defecto.

Entrypoint:
definido en el Dockerfile es la orden que imperativamente se ejecutará con el PID 1, lo que se ponga al final de la orden de docker run serán los ARGUMENTOS de la orden.

## Formas de definir el entrypoint:
+ En el Dockerfile: ENTRYPOINT ["/bin/bash"]  #En este caso para tener sesión de bash

+ En en propio docker run: 
```
docker run --rm --entrypoint /bin/bash -it javierf/ldap23
```

# Explicación ficheros
## Dockerfile:
Esta es la parte fácil, lo díficil estará en el fichero startup.sh. Aquí se instalarán los paquetes básicos para administrar además de los de LDAP (slapd y ldap-utils), se expondrá su puerto, y se especificará el startup.sh como ENTRYPOINT, por supuesto antes habiéndole dado permisos de ejecución.

## startup.sh
En el propio fichero explico lo que hace cada orden, y el objetivo de cada una.
Es "sencillamente" 4 funciones, y un case.


Por si falla algo, solucionarlo en modo interactivo:
```
$ docker run --rm --name ldap_entrypoint --entrypoint /bin/bash -it javierfb/ldap23:entrypoint

```
Si pone algo de que el nombre ya esta usado: docker ps -a + docker rm "nombre contenedor o ID"

## El resto:
+ edt-org.ldif: El fichero en formato ldif con la base de datos edt.org
+ slapd.conf: El fichero de configuración del ldap

# Ejecución:
Para ver los logs (el mensaje que dice que función se ha ejecutado)



## initdb:
**comanda:** 
$ docker run --rm --name ldap_entrypoint -d javierfb/ldap23:entrypoint initdb

```
$ docker run --rm --name ldap_entrypoint -d javierfb/ldap23:entrypoint initdb
$ docker ps
CONTAINER ID   IMAGE                        COMMAND                  CREATED         STATUS         PORTS     NAMES
61df9cc6dc14   javierfb/ldap23:entrypoint   "/opt/docker/startup…"   4 seconds ago   Up 3 seconds   389/tcp   ldap_entrypoint
$ docker exec -t ldap_entrypoint slapcat -n1 | grep dn
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: cn=Pau Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Anna Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Marta Mas,ou=usuaris,dc=edt,dc=org
dn: cn=Jordi Mas,ou=usuaris,dc=edt,dc=org
dn: cn=Admin System,ou=usuaris,dc=edt,dc=org
```


## slapd:
**comanda:**
$ docker run --rm --name ldap_entrypoint -d javierfb/ldap23:entrypoint slapd

```
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)$ docker run --rm --name ldap_entrypoint -d javierfb/ldap23:entrypoint slapd
f908e35126c650a150a4d1803c5ef2b8748ead2c4d7d8a4db450897b70a9dae2
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)$ docker ps
CONTAINER ID   IMAGE                        COMMAND                  CREATED         STATUS         PORTS     NAMES
f908e35126c6   javierfb/ldap23:entrypoint   "/opt/docker/startup…"   7 seconds ago   Up 6 seconds   389/tcp   ldap_entrypoint
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)$ docker exec -t ldap_entrypoint slapcat -n1
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)$ docker exec -t ldap_entrypoint slapcat -n0 | grep dn
dn: cn=config
dn: cn=module{0},cn=config
dn: cn=schema,cn=config
dn: cn={0}corba,cn=schema,cn=config
dn: cn={1}core,cn=schema,cn=config
olcAttributeTypes: {39}( 2.5.4.46 NAME 'dnQualifier' DESC 'RFC2256: DN qualifi
dn: cn={2}cosine,cn=schema,cn=config
dn: cn={3}duaconf,cn=schema,cn=config
dn: cn={4}dyngroup,cn=schema,cn=config
dn: cn={5}inetorgperson,cn=schema,cn=config
dn: cn={6}java,cn=schema,cn=config
dn: cn={7}misc,cn=schema,cn=config
dn: cn={8}nis,cn=schema,cn=config
dn: cn={9}openldap,cn=schema,cn=config
dn: cn={10}collective,cn=schema,cn=config
dn: olcDatabase={-1}frontend,cn=config
dn: olcDatabase={0}config,cn=config
dn: olcDatabase={1}mdb,cn=config
dn: olcDatabase={2}monitor,cn=config
olcAccess: {0}to *  by dn.base="cn=manager,dc=edt,dc=org" read  by * none

```


## start:
**A partir de aquí hay persistencia de datos**

```
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)$ docker run --rm --name ldap_entrypoint -h ldap_entrypoint -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap -it javierfb/ldap23:entrypoint start
Inicialización de javierfb/ldap23:entrypoint
Inicialización del servidor con persistencia de datos (especificar volumenes)
^Ca201037jf@i06:/var/tmp/201.1-docker-containers/Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)$ docker run --rm --name ldap_entrypoint -h ldap_entrypoint -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap -it javierfb/ldap23:entrypoint edtorg
Inicialización de javierfb/ldap23:entrypoint
Inicialización del servidor con persistencia de datos (especificar volumenes)
^Ca201037jf@i06:/var/tmp/201.1-docker-containers/Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)$ docker run --rm --name ldap_entrypoint -h ldap_entrypoint -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap -it javierfb/ldap23:entrypoint
Inicialización de javierfb/ldap23:entrypoint
Inicialización del servidor con persistencia de datos (especificar volumenes)
```

## slapcat 
> (Este solo he logrado que muestre el resultado haciendolo en interactivo ("-it" en lugar de "-d"))

```
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)$ docker run --rm --name ldap_entrypoint -h ldap_entrypoint -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap -it javierfb/ldap23:entrypoint slapcat | head -5
Inicialización de javierfb/ldap23:entrypoint
Mostrando datos de la base de datos especificada:
dn: cn=config
objectClass: olcGlobal
cn: config
write /dev/stdout: broken pipe
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)$ docker run --rm --name ldap_entrypoint -h ldap_entrypoint -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap -it javierfb/ldap23:entrypoint slapcat 0 | head -5
Inicialización de javierfb/ldap23:entrypoint
Mostrando datos de la base de datos especificada:
Mostrando base de datos 0
dn: cn=config
objectClass: olcGlobal
write /dev/stdout: broken pipe
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)$ docker run --rm --name ldap_entrypoint -h ldap_entrypoint -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap -it javierfb/ldap23:entrypoint slapcat 1 | head -5
Inicialización de javierfb/ldap23:entrypoint
Mostrando datos de la base de datos especificada:
Mostrando base de datos 1
dn: dc=edt,dc=org
dc: edt

```

## Invalido
```
a201037jf@i06:/var/tmp/201.1-docker-containers/Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)$ docker run --rm --name ldap_entrypoint -h ldap_entrypoint -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap -it javierfb/ldap23:entrypoint pepe
Inicialización de javierfb/ldap23:entrypoint
ERROR: Has puesto alguna opción inadecuada
Uso: 
			 
				# initdb → ho inicialitza tot de nou i fa el populate de edt.org

				# slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades

				# start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents

				# slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada

```

