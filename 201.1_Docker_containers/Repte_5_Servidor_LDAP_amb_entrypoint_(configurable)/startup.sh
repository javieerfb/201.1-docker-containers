#! /bin/bash
#
#(Si falla algo: solucionarlo en interactivo)
#$ docker run --rm --name ldap_entrypoint --entrypoint /bin/bash -it javierfb/ldap23:entrypoint

 
# initdb → ho inicialitza tot de nou i fa el populate de edt.org

# slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades

# start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents

# slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada 

function initdb(){

	# Esborrar els directoris de configuració i de dades respectivamente
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*

	# Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d  #slapd.conf está en el dir. contexto.

	#Esto es lo que mete los datos (a bajo nivel porque la orden empieza por slap)
	slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
	
	#Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
	chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

	#Encender el slapd (ldap) en detach
	/usr/sbin/slapd -d0
}

function slapd(){
	# !Básicamente es lo mismo que la anterior función, pero sin el slapadd

	# Esborrar els directoris de configuració i de dades respectivamente
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*

	# Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d  #slapd.conf está en el dir. contexto.

	#Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
	chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

	#Encender el slapd (ldap) en detach
	/usr/sbin/slapd -d0
}

echo "Inicialización de javierfb/ldap23:entrypoint"

case $1 in
	"initdb")
		echo "Inicialización + populate de edt.org"
		initdb;;
	"slapd")
		echo "Inicialización con base de datos vacia"
		slapd;;
	
	"slapcat")
		echo "Mostrando datos de la base de datos especificada:"
		if [ -n "$2" ];then	#-n hará que se cumpla el bloque sí ve que la cadena $2 no es un valor nulo
			case $2 in
				"1" | "0")
				echo "Mostrando base de datos $2"
				slapcat -n$2
				;;



				*)
				echo "slapcat -n $2 no es valido, que haces!?"
				exit 1
				;;
			esac
		else
			slapcat -n0	#En caso por defecto "sin especificar el número de BBDD, muestar la de configuración"
		fi
	;;		#<-- Cierre del slapcat

	"start" | "edtorg" | "")
		echo "Inicialización del servidor con persistencia de datos (especificar volumenes)"
		/usr/sbin/slapd -d0		#Inicia LDAP tal cual, sin nada
		;;

		*)
			echo -e "\nERROR: Has puesto alguna opción inadecuada"		#Pequeña ayuda para guiarme
			echo "Uso: 
			 
				# initdb → ho inicialitza tot de nou i fa el populate de edt.org

				# slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades

				# start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents

				# slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada"
			exit 1
		;;
		
esac


