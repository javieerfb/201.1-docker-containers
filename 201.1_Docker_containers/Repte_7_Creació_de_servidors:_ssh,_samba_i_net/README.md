# Enunciado
> Repte 7 Creació de servidors: ssh, samba i net
> Apunts: HowTo_ASIX_Docker.pdf capítol “Dockerfile”.
> Crear els servidors següents:
> ● Net: Implementa els serveis de xarxa echo, daytime, chargen, ftp, tftp i http.
> ● SAMBA: implementa un servidor samba stand alone.
> ● SSH amb autenticació contra el servidor ldap.

## Net

