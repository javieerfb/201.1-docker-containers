import time
import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

#Con el format(), se sustituyen los "curly brackets" {} del string por el resultado de la función 

@app.route('/')
def hello():
    count = get_hit_count()
    return 'HOLA ADIOS tardes por {}ª vez.\n'.format(count)


@app.route('/adios')
def bye():
    count = get_hit_count()
    return 'Hola por {}ª vez'.format(count)
