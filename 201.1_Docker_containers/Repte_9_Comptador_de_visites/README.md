# Repte 9 Comptador de visites
Enunciado:
> Apunts: HowTo_ASIX_Docker.pdf capítol “Docker compose”.
> Implementar l’exemple de la documentació del comptador de visites.

## Teoria
Los apuntes están en:
https://gitlab.com/edtasixm05/docker/getstarted/visites2

Página 145 del HowToDocker

## Explicaciones de los ficheros
### Explicación programa app.py

El flask es una herramienta que permite crear aplicaciones web rapidamente.
Aquí lo usaremos como lenguaje de desarrollo para crear aplicaciones web.

Las primeras 2 líneas:
```
app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)
```

1. Explicación:
Voy a construirme una aplicación (variable app).
Estoy inventando la variable cache que es para contactar con la aplicación

Últimas lineas:
```
@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello CAT MAN  World! I have been seen {} times.\n'.format(count)

```
2. Explicación:
Estas en la raiz, define una función que devuelve el número de veces que se consulta la web.

### Instalar los paquetes python (requirements.txt)
Se instalán a través de pip, y pip busca un archivo llamado requirements.txt,
por lo que creamos ese archivo, e instala los paquetes que haya (1 por línea)

### Revisión del Dockerfile
```
FROM python:3.7-alpine      #Un alpine que solo tiene la aplicación
WORKDIR /code
ENV FLASK_APP app.py        #Definimos que la aplicacción a ejecutar es "app.py"
ENV FLASK_RUN_HOST 0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers   
# apk add es el apt-get install del alpine

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["flask", "run"]        #La aplicación con ID 1 es flask, y el flask al iniciar inicializará la app

```

### Explicación docker-compose
```
version: '3'
services:
  web:
    build: .            #Fabrícará la imagen que tenemos al momento, sin nombre
    ports:
      - "5000:5000"
  redis:
    image: "redis:alpine"
```

## Pruebas
### Pruebas finales (Usar Redis)
En el navegador:
localhost:5000

En el redis:
```
$ docker exec -it comptador-redis-1 redis-cli
127.0.0.1:6379> get hits
"64"
127.0.0.1:6379> set hits 3
OK
127.0.0.1:6379> get hits
"3"
127.0.0.1:6379> set name 'pere'
OK
127.0.0.1:6379> get name
"pere"

```

### Segunda clase (Revisión)
Con docker history "container_name" ver el shell
```
 docker run --name some-redis -d redis
```

```
$ docker exec -it some-redis redis-cli
127.0.0.1:6379> set num 3
OK
127.0.0.1:6379> get num
"3"
127.0.0.1:6379> INCR num
(integer) 4
127.0.0.1:6379> INCR num
(integer) 5

```

### Teoria
$ docker pause comptador-redis-1

Con esto no podrá contactar con el redis, saldrá un error o se congelará la página

# Segunda parte
Añadimos volumenes:
Añado estas líneas:
```
    volumes:
      - .:/code
    environment:
      FLASK_ENV: development
```

Con esto se pueden hacer cambios en caliente, como cambiar lo que dice cada vez que se haga refresh.

Lo de actuar en modo "desarrollo" es para que cada vez que se modifique la aplicación se recarge automáticamente.

Tal y como dice al web oficial de flask: 
https://flask.palletsprojects.com/en/1.1.x/config/

> Setting FLASK_ENV to development will enable debug mode. flask run will use the interactive debugger and reloader by default in debug mode. To control this separately from the environment, use the FLASK_DEBUG flag.

(!) En teoría con e volumen habŕa persisnetcia de datos, lo que significa que aún apagando la máquina o eliminandola, se mantendrá el valor del contador.

## Crear otra "página" que diga otra cosa:
En el app.py:
Teniendo la app original, se copia y pega, se cambia la **app.route**, y cambio el return.
```
@app.route('/adios')
def bye():
    count = get_hit_count()
    return 'Adíos por {}ª vez'.format(count)
```

Para acceder a esta segunda app, en el navegador:
    **http://localhost:5000/adios**

## Persistencia
(!) Es importante diferencias los tipos de volumenes:
+ Named Volumes: Son volumenes donde se guarda el contenido de x directorio en dicho volumen
+ Bind Mounts: Lo uso en este caso para montar el directorio de contexto (con todos sus ficheros de configuración del container) en el propio container.

Ver el HowToASIX pág 64
