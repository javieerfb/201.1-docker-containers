# Apunts:
https://gitlab.com/edtasixm14/m14-projectes/-/raw/master/docs/HowTo-ASIX-API-REST.pdf

## Consultes API-REST
Fer consultes en la consola del navegador, idealment fer-les amb Chromium o Chrome, només fa falta anar a la web https://jsonplaceholder.typicode.com/ que es la que farem servir de probes, y premer Ctrl+Shift+J per obrir la consola.
La primera proba de consulta la vaig fer, així:
```
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then(response => response.json())
.then(json => console.log(json))
```

Com a OUTPUT vaig rebre el post amb id número 1.

# Prueba con Spotify API
https://developer.spotify.com/

M'he ficat en la página web, on una vegada en inicies sesió amb un compte de Spotify, pot començar a provar com funciona l'API.

Autorització:
Garanteiz a un usuari o aplicació permis d'accés a les dades de Spotify y altres caracteristiques.

1. Anema a https://developer.spotify.com/dashboard (estant log in)
2. Acceptem les condicions d'ús
3. Creo la aplicación:
    + App name:
    Mi aplicación de prueba
    + Redirect URI:    
    http://localhost:8000
    + Ús:
    Web API
    + Client ID (Auto-generada):
    Aquesta es l'identificador de l'APP creada.
    bfd8095603854592ab66e3b9151ac483
    + Client Secret:
    Es com un password que fa servir la nostra APP per autenticar-se y poder accedir als recursos de l'API-REST de Spotify.

## Preparar l'entorn
Encara no se com instalar-lo en l'ordinador de l'aula, però vaig a començar fent la prova en una máquina virtual.

L'app es realitza amb node.js que es un servidor web.

En el cas de debian: apt install -y nodejs

L'app de proba sería un simple fitxer .js, que executariem amb node, es ficaría en detach.
Pág 34 del HowTo


