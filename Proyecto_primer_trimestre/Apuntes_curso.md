# Què és una API en REST?

(Apuntes en sucio escritos en catalán(intento de) para prácticar)

Un API hi seria como el pont en una aplicació web i el seu servidor on té les dades que es mostren de cara al públic.
L'API ens permetría conectar diversos tipus de software o aplicacions, deuen tenir uan documentació clara per poderla implementar sense dificultat.
L'API deu ser oferta tant per als programes externs que podrán fer servir els serveis oferts, com al/la programador/a.

En el proyecte del curs de Cibernarium, ens proposen l'idea d'aprofitar la base de dades de Spotify, per agafar dades d'algun cantant que vulguem.
L'escenari que proposen:
Objectiu: Conexió efectiva entre l'usuari final i l'API de Spotify
Necessari: Programari de connexió integrat dins del propi code de la nostra aplicació que volem desenvolupar.
Un cop establerta la connexió, podrem fer peticions y rebre respotes aprofitant les dades de l'API. Aquestes dades que volem es troben en format d'objectes JSON.

Es important recordar el model client-servidor: el servidor es un passiu que espera peticions. 
El client es més bé un actiu, que envia peticions.

En el cas de les APIs, normalment com a resposta d'una petició donen documents JSON o XML, en comptes de en format HTML al que tant estem acostumats.

Anem a veure una mica més com funcionen les APIs, agafant com a exemple les de Google:
https://console.cloud.google.com/apis/library?hl=es-419&pli=1

En aquest a biblióteca d'APIs podem veure de tot, només fent clic a sobre de cualsevol d'aquestes podem veure una descripció de com funcionen y com implementar-les.
El que he fet ha sigut crear el meu projecte, anar al seu panell de control...

## API
L'API es un intermediari que permet que les teves eines extraigin, es comuniquin y treballin amb la informació disponibles que hagi sigut creada per altres desenvolupadors.

Cas d'ús:
El exemple més útil que se m'ocurre es el de Google Maps, imagina que vull fer servir en la meva aplicació un munt de mapes, aixó suposa un problema per la magnitud de les dades y demés, pero, puc fer servir els mapes que ya te fets el Google Maps, es a dir, informació d'un altra desenvolupador.

## Resumen definició API
Interficies per a que programes de software es comuniquin entre ells i compartin dades baix certs standars. El més utilitzat es REST, i normalment el format en el que s'envien les dades es JSON.

## Com funciona?
1. Trucada:

## APIs pçublica/privada
Pública: Tothom pot accedir
Privada: Requereix una autenticació, la primera vegada el servidor et retorna un TOKEN, es un object que conté totes les dades d'aquesta autenticació.
Cada cop que sol·licitis informació adicional, el servidor comprovará que el TOKEN segueix estant vigent, de forma que no tindrás que tornar a autenticar-te.

## APIs locals/remotes
Les remotes poden fer servir serveis web (un sistema de comunicació en una xarxa utilitzant en protocol HTTP), si fan servir serveis web, normalment es l'arquitectura REST.



## REST
Las APIs poden ser de diversos tipus, REST es una arquitectura, Representació de Transferencia d'Estat, que es fa servir per a la programació d'aplicacions web dinàmiques.

Per a que es pugui identificar un servidor web com a REST, ha de complir 6 restriccions 1 d'elles opcional:
+ Uniform Interface:
Defineix la interfície client-servidor.
- El recursos s'difentifiquen en la URI o URL
- Els missatges son descriptius, suficiente per a poder processarlos
...

+ Client-Server:
Són 2 coses independents.
El client no es preocupa d'emmagatzematge de dades.
El servidor no es preocupa de l'estat del client (Escalabilitat).

+ Stateless:
...




