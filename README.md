# M14 - Proyectos

## @javieerfb Sistemas microinformáticos y redes M14 2023-2024

### Documentación usada:
https://github.com/edtasixm06
https://gitlab.com/edtasixm06

https://gitlab.com/edtasixm05/docker
https://github.com/edtasixm05

**El documento de los retos:**
https://gitlab.com/edtasixm14/m14-projectes/-/blob/master/m14-projectes_reptes.pdf

**El documento donde se explica Docker:**
https://gitlab.com/edtasixm05/docker/-/blob/master/HowTo-ASIX-Docker.pdf

### Descripción:
+ 201.1_Docker_containers:
    + Repte_1_Sistema_operatiu_personalitzat
    + Repte_2_Servidor_detach_amb_propagació_de_port
    + Repte_3_Utilització_de_volums_en_un_servidor_Postgres
    + Repte_4_Servidor_LDAP_amb_persistència_de_dades
    + Repte_5_Servidor_LDAP_amb_entrypoint_(configurable)'
    + Repte_6_Networks,_Variables_i_Secrets
    + Repte_7_Creació_de_servidors:_ssh,_samba_i_net
    + Repte_8_Docker_compose:_iniciació
    + Repte_9_Comptador_de_visites
    + Repte_10_Rèpliques_amb_docker-compose.   

+ 201.2_Orquestració_amb_docker_swarm

