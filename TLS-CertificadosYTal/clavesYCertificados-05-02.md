 1892  mkdir TLS
 1893  cd TLS/
 1894  openssl genrsa -out server.key.pem
 1895  ls
 1896  cat server.key.pem 
 1897  openssl rsa --noout --text -in server.key.pem | less
 1898  # El text nos permite que nos explique la clave generada
 1899  openssl rsa --noout --text -in server.key.pem | less
 1900  openssl rsa -noout --text -in server.key.pem | less
 1901  openssl rsa --text -in server.key.pem | less
 1902  # El noout impide que al final vomite la clave
 1903  #openssl genrsa -out nombre
 1904  openssl genrsa -des3 -out key2.pem
 1905  head key2.pem 
 1906  # ahora con des3, estará la clave encriptada con nuestra passphrase
 1907  openssl rsa --text -in key2.pem | less
 1908  # Haciendo la orden anterior nos pedirá la passphrase para poder leerla
 1909  openssl rsa -in key2.pem -out key2no.pem
 1910  head key2no.pem 
 1911  # Con esto hemos hecho la misma clave, solo que ahora no está encriptada
 1912  openssl rsa -in server.key.pem -des3 -out server2.pem
 1913  # Hemos encriptado la clave"server.key.pem" con una passphrase y esta version ecnriptada se llama "server2.pem"
 1914  openssl rsa -in key2.pem -outform DER -out key2.der
 1915  head key2.der 
 1916  # Un .der es un binario, solo le hemos cambiado el formato a la clave
 1917  ls
 1918  openssl rsa -in key2.pem -pubout -out pubkey.pem
 1919  # Estamos extrayendo (Ya estaba creada/generada) la parte pública de la clave privada
 1920  openssl rsa --noout --text -pubin --in pubkey.pem | less
 1921  rm *
 1922  ls
 1923  # En esta parte ya haremos los CERTIFICADOS !!!!!!
 1924  openssl req -new -x509 -nodes -out servercert.pem -keyout serverkey.pem #La suborden req es una request, -x509 es un certificado, nodes que no nos pida la passphrase. Resumiendo la orden, que yo mismo me haré un certificado, y todo lo que necesito, es decir el certificado y la clave privada
 1925  ls
 1926  cat servercert.pem 
 1927  openssl x509 --noout -text -in servercert.pem | less
 1928  # Con la orden de encima podremos ver el certificado, el emisor, para que sirve, cuando caduca, ...
 1929  rm *
 1930  openssl genrsa -out -key.pem
 1931  # He generado 1 clave
 1932  ls
 1933  openssl req -new -x509 -days 365 -out server.pem -key -key.pem 
 1934  history 

1887  # El PUTO Common Name es lo que define el certificado, el certificado está emitido en nombre del Common Name
 1888  # Por lo que el certificado SOLO servirá si se le llama desde alli.
 1889  # Esto es lo que he contestado en la firma del certificado: Country Name (2 letter code) [AU]:ca
 1890  State or Province Name (full name) [Some-State]:cat
 1891  Locality Name (eg, city) []:bcn
 1892  Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
 1893  Organizational Unit Name (eg, section) []:inf
 1894  Common Name (e.g. server FQDN or YOUR name) []:pepet
 1895  Email Address []:edt.org

