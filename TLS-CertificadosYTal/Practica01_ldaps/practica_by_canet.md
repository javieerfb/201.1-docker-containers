https://github.com/edtasixm11/tls18/tree/master/tls18%3Aldaps

$ docker run --rm --name ldap.edt.org -h ldap.edt.org -d edtasixm11/tls18:ldaps

# Vampos a hacerlo en interactivo para ir haciendo pruebas

[root@ldap docker]# bash install.sh 
[root@ldap docker]# /sbin/slapd -u ldap -h "ldap:/// ldaps:/// ldapi:///" && echo "slapd Ok"
[root@ldap docker]# dnf install -y net-tools nmap vim procps less
[root@ldap docker]# nmap localhost

Starting Nmap 7.60 ( https://nmap.org ) at 2024-02-16 11:26 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000011s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 998 closed ports
PORT    STATE SERVICE
389/tcp open  ldap
636/tcp open  ldapssl

## Ahora quiero ver el certificado
[root@ldap docker]# dnf install -y openssl
[root@ldap docker]# openssl x509 -noout -text -in cacert.pem | less
[root@ldap docker]# openssl x509 -noout -text -in servercert.ldap.pem | less


## Tiene como autocargada el certificado de la CA
[root@ldap docker]# cat ldap.conf | grep CA
#TLS_CACERTDIR /etc/openldap/certs
TLS_CACERT /opt/docker/cacert.pem

protocolo start tls, sobre unaconexión de texto plano ambos usuarios deciden encriptar la comunicación

[root@ldap docker]# ldapsearch -x -LLL -H ldap://ldap.edt.org -s base dn
dn: dc=edt,dc=org

[root@ldap docker]# ldapsearch -x -LLL -Z -H ldap://ldap.edt.org -s base dn
dn: dc=edt,dc=org

### La dooble Z: Solo se puede hacer la conexión sobre TLS
[root@ldap docker]# ldapsearch -x -LLL -ZZ -H ldap://ldap.edt.org -s base dn
dn: dc=edt,dc=org

(!) No cambia de puerto, lo hace por el mismo puerto, pero es como si hablase en chino

La Z es para conectar por un puerto no privilegiado y 

### Añadiendo la s (ldaps)
[root@ldap docker]# ldapsearch -x -LLL -ZZ -H ldaps://ldap.edt.org -s base dn 
ldap_start_tls: Operations error (1)
	additional info: TLS already started

## Nos funcionará con varios nombres:
[root@ldap docker]# ldapsearch -x -LLL -H ldaps://172.17.0.2 -s base dn
dn: dc=edt,dc=org


# Otros nombres (El certificado no está hecho para ellos)
[root@ldap docker]# cat /etc/hosts
127.0.0.1	localhost lo.cat
::1	localhost ip6-localhost ip6-loopback
fe00::0	ip6-localnet
ff00::0	ip6-mcastprefix
ff02::1	ip6-allnodes
ff02::2	ip6-allrouters
172.17.0.2	ldap.edt.org ldap ip.cat


[root@ldap docker]# ldapsearch -x -LLL -Z -H ldap://ip.cat -s base dn
ldap_start_tls: Connect error (-11)
	additional info: TLS: hostname does not match CN in peer certificate

## Observar mejor el problema
[root@ldap docker]# ldapsearch -d1 -x -LLL -Z -H ldap://ip.cat -s base dn

# Vamos a cambiar un fhicero, ponemos al final otros nombres permitidos
[root@ldap docker]# cat ext.alternate.conf 
basicConstraints=CA:FALSE
extendedKeyUsage=serverAuth
subjectAltName=IP:172.17.0.2,IP:127.0.0.1,email:copy,DNS:ip.cat,DNS:lo.cat

# Generamos el Certificado nuevo
openssl x509 -CAkey cakey.pem -CA cacert.pem -req -in serverreq.ldap.pem -days 3650 -CAcreateserial -extfile ext.alternate.conf -out servercert.ldap.pem
Signature ok
subject=/C=ca/ST=barcelona/L=barcelona/O=edt/OU=informatica/CN=ldap.edt.org/emailAddress=ldap@edt.org
Getting CA Private Key

## Movemos el nuevo certificado donde se debe
cp servercert.ldap.pem /etc/openldap/certs/

## Miramos que efectivamente ha cambiado los nombres permitidos
openssl x509 -noout -text -in servercert.ldap.pem


# En otro cliente:
[root@ldap docker]# ps ax
    PID TTY      STAT   TIME COMMAND
      1 pts/0    Ss+    0:00 /bin/bash
     18 pts/1    Ss     0:00 /bin/bash
     54 ?        Ssl    0:00 /sbin/slapd -u ldap -h ldap:/// ldaps:///
    129 pts/1    R+     0:00 ps ax

[root@ldap docker]# kill -1 54

[root@ldap docker]# /sbin/slapd -u ldap -h "ldap:/// ldaps:/// ldapi:///" 

## El resto sería verificar

# Otra cosa mirar el certificado de google, para ver que cosas se hacen bien:
[root@ldap docker]# openssl s_client -connect www.gmail.com:443


