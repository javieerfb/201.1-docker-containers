# Pràctica 01: ldaps
M14 projectes Curs 2023 - 2024

**Enunciado**
> Implementar un servidor amb servei LDAP segur ldaps.
> Primer: Crear
> ● Crear una entitat certificadora Veritat Absoluta.
> ● Generar un certificat de servidor a nom de ldap.edt.org.
> ● Configurar el servidor ldap per actuar com a servidor ldaps.
> ● Configurar un client PAM per fer consultes contra el servidor LDAP (configurar el certificat de CA)
> Segon: Verificar
> ● Verificar la connexió ldap amb el servidor LDAP.
> ● Verificar la connexió ldaps amb el servdor LDAP.
> ● Verificar el funcionament de l’opció de ldapsearch -z.
> ● Verificar el funcionament de l’opció de ldapsearch -Z.
> Tercer: Alternate Names
> ● Generar de nou el certificat del servidor ampliant-lo amb una llista de noms alternatius del servidor:
>   ○ Localhost, 127.0.0.1, <ip-container>, myldap.edt.org.
> ● Vetrificar la connexió des d’un host client PAM i des de dins del propi servidor per a cada cas.

Estoy siguiendo esto (Pág 144 - ):
**https://gitlab.com/edtasixm06/m06-aso/-/blob/main/UF1_1_ldap/MASTERING_OPENLDAP_CONFIGURING_SECURING_AND_INTE.pdf?ref_type=heads**

o esto que es mas senzillo:
**https://ubuntu.com/server/docs/service-ldap-with-tls**

# Inicialización
```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --network 2hisx -d javierfb/tls23:ldaps

$ docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -d javierfb/pam23:ldap

Hay que meter el certificado en el cliente!!!
y configurar que el cliente lo use


```

## Preparación:
1. iniciar el servidor ldap

$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d javierfb/ldap23:latest

2. Instalar paquetes (Añadir esta orden al Dockerfile)
apt update && apt install gnutls-bin ssl-cert -y

3. 

## Crear
1. Crear la entidad certificadora (CA)   (La pass phrase es **jupiter**)
```
root@ldap:/opt/docker# openssl req -new -x509 -days 365 -out cacert.crt -keyout cakey.key
.....+......+.+.....+...+.......+......+...+......+...+...+..+......+............+.......+...+..+.+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*..+.........+....+......+...+.....+...+...+.+........+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*...........+.........+..+....+......+......+.................+.+......+......+..+......+......+......+.........+.+............+...+..+.......+........+.+..+....+........+...+....+......+.................+............+..........+.....+......+.........+.+...+..+.......+...........+.........+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
....+......+.......+..+..........+.....+.+......+.........+......+.....+.+........+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*......+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.+.........+..............+..........+..+.......+..+.+...+..+....+...+..+....+.....+.........+.+.....+....+......+......+.......................+...+.......+..+....+.....+....+.....+...+.......+..+....+.....+....+...+..+.......+......+..+.+.....+.......+..+.+..+...+.......+.........+......+............+..+....+......+........+.+.....+..........+..+.........+...+.+......+........+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:cat
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Veritat Absoluta
Organizational Unit Name (eg, section) []:veritat
Common Name (e.g. server FQDN or YOUR name) []:VeritatAbsoluta
Email Address []:veritat@edt.org

```

2. Generar un certificado de servidor a nombre de ldap.edt.org, (en este caso será firmado por la CA, pero puede ser autofirmado)
```
#### Genero clave del servidor y solicitud de firma (request)

openssl genrsa -out ldap_server.key 2048

#### La solicitud:
root@ldap:/opt/docker# openssl req -new -key ldap_server.key -out ldap_server_cert_request.pem
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:cat
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
Organizational Unit Name (eg, section) []:org
Common Name (e.g. server FQDN or YOUR name) []:ldap.edt.org
Email Address []:ldap@ldap.org

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:jupiter
An optional company name []:



#### La firma por la CA de la solicitud:

root@ldap:/opt/docker#  openssl x509 -CA cacert.crt -CAkey cakey.key -req -in ldap_server_cert_request.pem -out ldap_server.crt -days 365 -sha256 -CAcreateserial
Certificate request self-signature ok
subject=C = ca, ST = cat, L = bcn, O = edt, OU = org, CN = ldap.edt.org, emailAddress = ldap@ldap.org
Enter pass phrase for cakey.key:

```

3. Configurar el servidor ldap para actuar como servidor ldaps:
Hay que editar el **slapd.conf** para cargarlos:

```
# Ubicacion de certificados y sus respectivas claves (TLS)
TLSCACertificateFile        /opt/docker/cacert.crt
TLSCertificateFile          /opt/docker/ldap_server.crt
TLSCertificateKeyFile       /opt/docker/ldap_server.key
```

También editar el **startup.sh**
```
#Es necesario que a la hora de iniciar el servicio ldap (slapd), también escuche por el puerto seguro 636 de LDAPS:
/usr/sbin/slapd -d0 -h "ldap:// ldaps:// ldapi://"

```

Además editar el **ldap.conf** le añadimos para que use el certificado de la CA que validará el certificado del servidor.
```
# TLS certificates (needed for GnuTLS)
TLS_CACERT	/opt/docker/cacert.crt

```

(!) Acordarse de que en el startup.sh se debe de copiar este fichero de /opt/docker (que hemos modificado) a su ruta original:
```
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
```

4. Configurar un cliente pam para hacer consultas contra el servidor LDAP

Iniciar el cliente PAM:
```
docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -d javierfb/pam23:ldap
```

Habrá que editar el /etc/ldap/ldap.conf para añadir el certificado de la CA, pero primero lo copiaré de un docker a otro:
(Lo he copiado manualmente)

```
root@pam:/opt/docker# hostname 
pam.edt.org
root@pam:/opt/docker# tail /etc/ldap/ldap.conf 
URI	ldap://ldap.edt.org

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/opt/docker/cacert.crt

```

## Verificar: Pruebas con el cliente:

1. Verificar conexión con LDAP:
```
root@pam:/opt/docker# ldapsearch -x  -H ldap://ldap.edt.org | head -20
# extended LDIF
#
# LDAPv3
# base <dc=edt,dc=org> (default) with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# edt.org
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

# maquines, edt.org
dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux

```

2. LDAPS:
```
root@pam:/opt/docker# ldapsearch -x  -H ldaps://ldap.edt.org | head -20
# extended LDIF
#
# LDAPv3
# base <dc=edt,dc=org> (default) with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# edt.org
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

# maquines, edt.org
dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux

```

3. Verificar con opción -z
```
root@pam:/opt/docker# ldapsearch -x -LLL -z -H ldaps://ldap.edt.org
Unable to parse size limit "-H"

```

4. Verificar con opción -Z
```
root@pam:/opt/docker# ldapsearch -x -LLL -Z -H ldaps://ldap.edt.org | head
ldap_start_tls: Operations error (1)
	additional info: TLS already started
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux

```

### Troubeshooting
Tenía un problema con unas líneas del slapd.conf
Concretamente estas:

```
TLSVerifyClient       never
TLSCipherSuite        HIGH:MEDIUM:LOW:+SSLv2
```

Para averiguar esto:
1. podemos hacer uno por uno las ordenes del startup.sh, 
si todas se hacen bien, 

2. pasamos a iniciar el servicio en detach, si esto no va entonces, lo hacemos en modo debug (9) para que vervalize los errores.

/usr/sbin/slapd -d9 -h "ldap:// ldaps:// ldapi://" 2>err.log

3. La buena rpáctica es reenviar los errores a un fichero, y luego leer el fichero, en mi caso al final ponia que una directiva (del slapd.conf) no funcionaba.

# Tercero: Alternate Names
1. Crearemos un fichero donde especificaremos entre otras la IP del docker y el nombre que queremos que resuelva en DNS como nuestro servidor.
```
root@ldap:/opt/docker# cat ext.alternate.conf
basicConstraints=CA:FALSE
extendedKeyUsage=serverAuth
subjectAltName=IP:172.18.0.2,IP:127.0.0.1,email:copy,DNS:myldap.edt.org

```

2. Generamos un nuevo certificado
```
root@ldap:/opt/docker# openssl x509 -CAkey cakey.key -CA cacert.crt -req -in ldap_server_cert_request.pem -days 3650 -CAcreateserial -extfile ext.alternate.conf -out ldap_server.crt
Certificate request self-signature ok
subject=C = ca, ST = cat, L = bcn, O = edt, OU = org, CN = ldap.edt.org, emailAddress = ldap@ldap.org
Enter pass phrase for cakey.key:

```

3. En el cliente PAM
