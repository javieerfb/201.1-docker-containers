#!/bin/bash
#Consultar: Github edtasixm0/ldap22/ldap22:base/README.md
# Esborrar els directoris de configuració i de dades respectivamente
rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf

/usr/sbin/slapd -d0 -h "ldap:// ldaps:// ldapi://"

#<<CONSULTAS
#Consulta: "ldapsearch -x -LLL -b 'dc=edt,dc=org' -s base"

#Consulta desde fuera del docker: (Con la -H protocolo://IP del docker)
#ldapsearch -x -H ldap://172.17.0.2 -LLL -b 'dc=edt,dc=org' -s base
#CONSULTAS
