# ldap23:grups

## Inicialización
```
$ docker run --rm --name grups.edt.org -h grups.edt.org --net 2hisx -p 389:389 -d javierfb/ldap23:grups

# Comprobar:
$ ldapsearch -x -H ldap://localhost:389 -b 'dc=edt,dc=org'

o

$ docker exec grups.edt.org slapcat -n1

```


## Pasos
Base de datos edt.org

1. Todos los usuarios se identifican en su "dn" por su "uid".

2. Crear la Unidad organizativa (ou) "grups", donde irán todos los grupos.
```
dn: ou=grups,dc=edt,dc=org
ou: groups
description: Container per a grups
objectclass: organizationalunit
```

### Grupos y sus usuarios
3. Crear todos los grupos como entidades PosixGroup.

PosixGroup tiene la siguiente estructura:
```
dn: cn=1hiaw,ou=grups,dc=edt,dc=org
cn: 1hiaw
gidNumber: 614
objectclass: posixgroup

OPCIONALES:
description: grupo de la clase 1hiaw    (No lo voy a poner)
memberUid: user1           (Este tendré que usarlo para asignar los PosixAccount al PosixGroup)
memberUid: ...
userPassword: user1        (No lo voy a poner)
```

#### Grupos
Sintaxi: cn / gid
+ professors / 601
+ alumnes / 600
+ 1asix / 610
+ 2asix / 611
+ sudo / 27
+ 1wiam / 612
+ 2wiam / 613
+ 1hiaw / 614

#### Usuarios
Aclaraciones:
+ Todos los usuarios con gidNumber: 100, pasan a ser gidNumber 601 (Lo cual no tiene sentido? ya que serían casi todos profesores...)

He considerado mejor distribuir más los grupos, hasta que se aclare.
Por eso pongo los usuarios y sus grupos más adelante, a modo de listado:

+ El usuario "admin" pasa a tener el gidNumber 27 (sudo)


Sintaxi:    uid    /   gidNumber
uid: pau		601	(profes)
uid: pere		601

uid: anna		600	(alumnos)
uid: marta		600

uid: jordi		601

uid: admin          (27, es importante que pertenezca al grupo sudo)

uid: user0		610	(1asix)
uid: user1		610
uid: user2		610
uid: user3		610
uid: user4		610

uid: user5		611	(2asix)
uid: user6		611
uid: user7		611
uid: user8		611
uid: user9		611

uid: user10		612	(1wiam)

uid: user11		613	(2wiam)

### Esquemas
4. Para averiguar que esquemas són necesarios:
https://gitlab.com/edtasixm06/m06-aso/-/blob/main/UF1_1_ldap/HowTo-ASIX_LDAP.pdf?ref_type=heads
Página 56

#### Conocer los esquemas
Lo primero es saber que:
+ Los ficheros de esquemas generados están donde dice el slapd.d
+ Y los esquemas como tal, se han generado con el "slaptest" en el directorio: **/etc/ldap/slapd.d/cn=config/cn=schema**
```
root@grups:/etc/ldap/slapd.d/cn=config/cn=schema# tree
.
|-- cn={0}corba.ldif
|-- cn={10}collective.ldif
|-- cn={1}core.ldif
|-- cn={2}cosine.ldif
|-- cn={3}duaconf.ldif
|-- cn={4}dyngroup.ldif
|-- cn={5}inetorgperson.ldif
|-- cn={6}java.ldif
|-- cn={7}misc.ldif
|-- cn={8}nis.ldif
`-- cn={9}openldap.ldif

1 directory, 11 files
```
+ En la propia documentación de OpenLdap, dice cuales esquemas son indispensables:
https://www.openldap.org/doc/admin22/schema.html

    Los que vienen por defecto:
    core.schema
    cosine.schema
    inetorgperson.schema
    misc.schema
    nis.schema
    openldap.schema

#### Consultar esquemas
+ A partir de aquí podemos ir buscando donde se declaran las clases y atributos que usamos en nuestra base de datos, haciendo "grep <elemento> *".

Por ejemplo, saber que schema tiene la clase "posixGroup":
```
root@grups:/etc/ldap/slapd.d/cn=config/cn=schema# ls
'cn={0}corba.ldif'	  'cn={2}cosine.ldif'	 'cn={5}inetorgperson.ldif'  'cn={8}nis.ldif'
'cn={10}collective.ldif'  'cn={3}duaconf.ldif'	 'cn={6}java.ldif'	     'cn={9}openldap.ldif'
'cn={1}core.ldif'	  'cn={4}dyngroup.ldif'  'cn={7}misc.ldif'

root@grups:/etc/ldap/slapd.d/cn=config/cn=schema# slapcat -n0 | grep posixGroup
olcObjectClasses: {2}( 1.3.6.1.1.1.2.2 NAME 'posixGroup' DESC 'Abstraction of 
```

#### Esquemas que dejaré
+ Conclusión, solo dejaré los esquemas predefinidos:
```
$ cat slapd.conf 

# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

#include		/etc/ldap/schema/corba.schema
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
#include		/etc/ldap/schema/duaconf.schema
#include		/etc/ldap/schema/dyngroup.schema
include		/etc/ldap/schema/inetorgperson.schema
#include		/etc/ldap/schema/java.schema
include		/etc/ldap/schema/misc.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema
#include		/etc/ldap/schema/ppolicy.schema
#include		/etc/ldap/schema/collective.schema

```

### Escuchar por todos los protocolos
5. Modificar el startup.sh para que el servicio ldap escuche por todos los protocolos ldap, ldapi y ldaps.

En la última línea del fichero startup.sh, en la que se ejcuta el servicio slapd en detach, hay que poner lo siguiente:

```
/usr/sbin/slapd -d0 -h "ldap:// ldaps:// ldapi://"
```

Por defecto slapd solo escucha ldap por el puerto 389.

Pero con la opción -h se pueden especificar otras URLS a servir.
Fuente: https://linux.die.net/man/8/slapd
> -h URLlist
>    slapd will by default serve ldap:/// (LDAP over TCP on all interfaces on default LDAP port). That is, it will bind using INADDR_ANY and port 389. The -h option may be used to specify LDAP (and other scheme) URLs to serve. For example, if slapd is given -h "ldap://127.0.0.1:9009/ ldaps:/// ldapi:///"



# Actualización 7/02/2024:
En teoría ya están los grupos tal y como los quiere el Canet.
Además he subido está imagen como "latest":
```
$ docker tag javierfb/ldap23:grups javierfb/ldap23:latest
$ docker push javierfb/ldap23:latest
```
