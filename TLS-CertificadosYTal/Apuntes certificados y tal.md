https://gitlab.com/edtasixm11/m11-sad/-/blob/master/UF2_Seguretat_activa_i_acc%C3%A9s_remot/TLS_SSL/HowTo-ASIX-draft-Certificats-Digitals.pdf?ref_type=heads


Vamos a crear una CA, para ello también crearemos un certificado o algo asi, no me entero ni del clima.

PS1=" Veritat$   "
openssl req -new -x509 -days 365 -out cacert.pem -keyout cakey.pem #Con keyout también me creará la ckave que necesito para el certificado

1900  #Ver los datos de la clave
 1901  openssl rsa --noout --text -in cakey.pem 

 1904  openssl x509 --noout --text -in cacert.pem #Ahora es x509 porque es un certificado


# Esat clave es de otra persona
 Veritat$   openssl genrsa -out serverkey.pem 2048      
 
# Algo del Veritat (CA)
```
Veritat$   openssl req -new -x509 -days 365 -out cacert.pem -keyout cakey.pem #Con keyout también me creará la ckave que necesito para el certificado
.....+...+.+..+...+....+...+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*...+....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.......+...+...+...+......+.+...............+.....+....+.....+....+.....+.......+.....+....+...+...+..+......+.......+..+.+.....+.......+..............+.+...+..+.......+..+.+..............+......+....+.........+.....+.+...+..+..................+...+...+.+...+...+.....+...+....+...+...+.........+...............+..+...+......+.........+...+...+.+........+.+...........+....+...........+......+....+.....+....+.........+..+.+..+...+.............+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.+.+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.+...+...+...............+....+........+.+.........+......+.....+......+....+.........+.....+......+.+...+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.........+.+........+.......+........+.......+..+.......+...+..+...+...............+...+....+.....+.+...+............+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:cat
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Veritat Absoluta
Organizational Unit Name (eg, section) []:veritat
Common Name (e.g. server FQDN or YOUR name) []:VeritatAbsoluta    
Email Address []:veritat@edt.org
```
 
 
# Petición de certificación
    $   openssl req -new -key serverkey.pem -out request.pem     #Esto será una petición de certificación, para ello paso mi clave privada
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:cat
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
Organizational Unit Name (eg, section) []:inf
Common Name (e.g. server FQDN or YOUR name) []:server.edt.org
Email Address []:server@edt.org

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:jupiter
An optional company name []: 
    $   ls
cacert.pem  cakey.pem  clavesYCertificados-05-02.md  request.pem  serverkey.pem

## Así vemos la petición (se puede ver como ha obtenido de la clave privada la pública)
openssl req --noout --text -in request.pem

## Firmar la petición de certificación por CA
A la hora de firmarla, nos pedirá la pass phrase nos pedirá la clave privada del CA

```
 Veritat$   openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in request.pem -out servercert.pem -CAcreateserial 
Certificate request self-signature ok
subject=C = ca, ST = cat, L = bcn, O = edt, OU = inf, CN = server.edt.org, emailAddress = server@edt.org
Enter pass phrase for cakey.pem:
 Veritat$   ls
cacert.pem  cacert.srl  cakey.pem  clavesYCertificados-05-02.md  request.pem  servercert.pem  serverkey.pem

```


## Ver el certificado:

Veritat$   openssl x509 --noout --text -in servercert.pem 
        Issuer: C = ca, ST = cat, L = bcn, O = Veritat Absoluta, OU = veritat, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
        Validity
            Not Before: Feb  7 11:44:33 2024 GMT
            Not After : Mar  8 11:44:33 2024 GMT
        Subject: C = ca, ST = cat, L = bcn, O = edt, OU = inf, CN = server.edt.org, emailAddress = server@edt.org


# Vamos a generar otro para pruebas:
openssl x509 -CA cacert.pem -CAkey cakey.pem -days 3650 -req -in request.pem -out s2.pem -CAcreateserial
openssl x509 --noout --text -in s2.pem | less

# Otro mas

## Hacemos un fichero
Especifico que ese certificado usara autenticación por email 

```
 Veritat$   vim ca.conf
 Veritat$   cat ca.conf 
# Pág 5 de lo apuntes
basicConstraints = critical,CA:FALSE
extendedKeyUsage = serverAuth,emailProtection
```

## Lo hacemos
Veritat$   openssl x509 -CA cacert.pem -CAkey cakey.pem -days 3650 -extfile ca.conf -req -in request.pem -out s3.pem -CAcreateserial 
Certificate request self-signature ok
subject=C = ca, ST = cat, L = bcn, O = edt, OU = inf, CN = server.edt.org, emailAddress = server@edt.org
Enter pass phrase for cakey.pem:

                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: critical                  
                CA:FALSE
            X509v3 Extended Key Usage: 
                TLS Web Server Authentication, E-mail Protection            <<<<<<< yey
                
                
## Podemos buscar datos concretos del certificado
 Veritat$   openssl x509 --noout -issuer -subject -dates -in s3.pem 
issuer=C = ca, ST = cat, L = bcn, O = Veritat Absoluta, OU = veritat, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
subject=C = ca, ST = cat, L = bcn, O = edt, OU = inf, CN = server.edt.org, emailAddress = server@edt.org
notBefore=Feb  7 11:52:17 2024 GMT
notAfter=Feb  4 11:52:17 2034 GMT


