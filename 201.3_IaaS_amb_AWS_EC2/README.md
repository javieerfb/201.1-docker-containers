# Apuntes: 
https://gitlab.com/edtasixm05/aws/-/raw/main/HowTo-ASIX-Amazon-AWS-EC2-Projectes.pdf?ref_type=heads

- HowTo (Teoria) = zzzz
- Projectes : Lo que vamos a seguir

En el AWS:
Usaremos el curso:
AWS Academy Cloud Foundations [64859]

Módulo 6 --> Lab 3
Hay que bajar al finaol del todo he ir danto en "I agree", luego reiniciar y saldrá todo.

# Como empezar
AWS Academy Learner Lab [64860] > Modules > AWS Academy Learn > Launch AWS Academy Learner Lab

Le doy a Start Lab, tardará unos 3 minutos

(!) Amazon está compartimentado en regiones, lo ideal es siempre usar la de Londres cuando paguemos.

Partes del AWS:
+ El panel de administración de AWS

# Projecte 2 Crear una VM
Crear una instancia basada en una imagen AMI(Amazon Machine Image) de Amazon. 
Importante que sea "Free tier elegible"

Instance type: t2.micro

Key pair:
Create key pair
Crearé un par de claves para conectarme luego por ssh
Nombre: keypair-2hisx
RSA y .pem

Moveremos el par de claves a .ssh de nuestro home, si no tenemoseste directorio, basta con hacer ssh a nosotros mismos.
a201037jf@i06:~$ mv keypair-2hisx.pem .ssh/
a201037jf@i06:~$ chmod 600 .ssh/keypair-2hisx.pem
a201037jf@i06:~$ file .ssh/keypair-2hisx.pem 
.ssh/keypair-2hisx.pem: PEM RSA private key

Si hicieramos ls -l de la clave, se ve como tiene demasiados permisos abiertos

Si hicieramos un cat del fichero, veriamos que es el contenido de la clave en base 64

a201037jf@i06:~$ openssl rsa --noout --text -in .ssh/keypair-2hisx.pem 
Con esto vemos la clave

Network settings:
Create security group
Allow SSH trafuc frin Anywhere

Launch Instance:
Cuando salga el "Success", hay que hacer clic en la instancia (En el ejemplo siguiente, sería i-05b5a...): 
Successfully initiated launch of instance (i-05b5a1ac21686138f)


## Importante
para conectarse por ssh a nuestras instancias, lo ideal es tenerlas apuntadas en un papel, y así no hay que ir entrando al panel de administración.

El usuario por defecto es: ec2-user
Se pueden ver otros ejemplos en el fichero de Projectos pág 32
https://gitlab.com/edtasixm05/aws/-/raw/main/HowTo-ASIX-Amazon-AWS-EC2-Projectes.pdf?ref_type=heads

Voy a usar la clave generada para meterme, con la opción -i en ssh
```
a201037jf@i06:~$ ssh -i .ssh/keypair-2hisx.pem ec2-user@52.91.155.238
   ,     #_
   ~\_  ####_        Amazon Linux 2023
  ~~  \_#####\
  ~~     \###|
  ~~       \#/ ___   https://aws.amazon.com/linux/amazon-linux-2023
   ~~       V~' '->
    ~~~         /
      ~~._.   _/
         _/ _/
       _/m/'
[ec2-user@ip-172-31-41-112 ~]$ cat /etc/os-release 
NAME="Amazon Linux"
VERSION="2023"
ID="amzn"
ID_LIKE="fedora"
VERSION_ID="2023"
PLATFORM_ID="platform:al2023"
PRETTY_NAME="Amazon Linux 2023"
ANSI_COLOR="0;33"
CPE_NAME="cpe:2.3:o:amazon:amazon_linux:2023"
HOME_URL="https://aws.amazon.com/linux/"
BUG_REPORT_URL="https://github.com/amazonlinux/amazon-linux-2023"
SUPPORT_END="2028-03-15"
[ec2-user@ip-172-31-41-112 ~]$ 
[ec2-user@ip-172-31-41-112 ~]$ free -h
               total        used        free      shared  buff/cache   available
Mem:           949Mi       133Mi       577Mi       2.0Mi       238Mi       676Mi
Swap:             0B          0B          0B
[ec2-user@ip-172-31-41-112 ~]$ df -h
Filesystem      Size  Used Avail Use% Mounted on
devtmpfs        4.0M     0  4.0M   0% /dev
tmpfs           475M     0  475M   0% /dev/shm
tmpfs           190M  2.9M  188M   2% /run
/dev/xvda1      8.0G  1.5G  6.5G  19% /
tmpfs           475M     0  475M   0% /tmp
/dev/xvda128     10M  1.3M  8.7M  13% /boot/efi
tmpfs            95M     0   95M   0% /run/user/1000

```

## Salir
1. Hay que terminar la instancia
2. Hay que terminar el Lab

