# HowTo de Vagrant
Usaré este documento un poco como chuleta

# Imagenes (box)
$ vagrant box 
     add
     list
     outdated
     prune
     remove
     repackage
     update

## Providers
Las boxes de Vagrant solo funcionan con 1 provider, 1 box debe instalarse para 1 provider.
Puedes tener 2 boxes con el mismo nombre, siempre que tengan distinto provider.
Fuente: https://developer.hashicorp.com/vagrant/docs/providers/basic_usage

## Provisioning
Las acciones que hace

--provision
Para aplicar otra ver el provisioning (Vagrantfile)

$ vagrant up --provision

Teniendo en cuenta claro, que la máquina ya estaba encendida


Virtualbox cuando crea una máquina tendrá la IP 10.0.2.15 por defecto.

Puede hacer varias con la misma IP. ya que por defecto estan aisladas, solo tienen acceso a Internet.


### Para generar un Vagrantfile de la box especificada (minimal es para que no tenga comentarios y demas)

vagrant init --minimal generic/alpine38


