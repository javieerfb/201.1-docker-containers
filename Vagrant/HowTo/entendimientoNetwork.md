# Pág 46 - del HowTo Vagrant
# La documentación oficial, bastante bien, la verdad:
https://developer.hashicorp.com/vagrant/docs/networking/private_network

Por defecto las máquinas con vagrant estan aisladas

  config.vm.network "private_network", ip: "192.168.56.66", netmask: "255.255.255.192"

  config.vm.network "private_network", ip: "192.168.56.67", netmask: "255.255.255.192"

Ahora podremos hacernos ping entre las maquinas

Fijemonos que además podemos hacer ping desde nuestro host anfitrion a las boxes.
Tenemos hasta nuestra porpia interfaz:

```
vboxnet0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 0a:00:27:00:00:00 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.65/26 brd 192.168.56.127 scope global vboxnet0
       valid_lft forever preferred_lft forever
    inet6 fe80::800:27ff:fe00:0/64 scope link 
       valid_lft forever preferred_lft forever
```


# DHCP
  config.vm.network "private_network", type: "dhcp"

Nos vuelve a crear una interfaz de red en la que somos nosotros la IP de red:

```
vboxnet1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 0a:00:27:00:00:01 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.1/24 brd 192.168.56.255 scope global vboxnet1
       valid_lft forever preferred_lft forever
    inet6 fe80::800:27ff:fe00:1/64 scope link 
       valid_lft forever preferred_lft forever
```

## Bridge
La misma interfaz que nosotros tenemos, en nuestro caso eno1, simulará que son ,áquinas que estan dentro del aula.


