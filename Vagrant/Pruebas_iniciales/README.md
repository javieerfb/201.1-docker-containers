Apuntes:
https://gitlab.com/edtasixm14/m14-projectes/-/blob/master/docs/HowTo%20ASIX%20Vagrant.pdf?ref_type=heads

Ejercicios: 
https://gitlab.com/edtasixm14/m14-projectes/-/blob/master/m14-projectes_reptes.pdf?ref_type=heads

Clase 21/02/2024
Nosotros tenemos un Host. y podemos desplegar una instancia en AWS, y tal.

Métodos:
1. Web EC2
2. AWS CLI
3. Terraform


En nuestro ordenador sabemos desplegar:
+ containers / LXC (linux containers)
+ Máquinas virtuales


Existe un concepto que se llama Vagrant, y está en una capa inferior a las mźaquinas virtuales.

descripción y despliege de máquinas virtuales y dockers.

También está Ansible. Pero Vagrant sobretodo lo peta, su empresa es HashiCorp

# Vagrant
mkdir lab1
cd lab1

Vagrantfile, solo hay 1 "Vagrantfile" por directorio

**Provider** es quien gestiona las máquinas virtuales (el gestor de estas)
En mi caso usual sería el VirtualBox

Vagrant como tal es solo un sistema/mecanismo

**Provisioning**: 
De que proveeremos a la máquina, en el caso del docker es Dockerfile, por ejemplo.

Que mecanismos hay de provisioning?
+ Darle ordenes
+ Decirle que ejecute scripts
+ Qué haga algo con Ansible

# Parte práctica
```
$ vagrant -v
Vagrant 2.4.1

## Bajate el fichero de configuración de debian ()
$ vagrant init hashicorp/bionic64
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.

## Inicio 
a201037jf@i06:/var/tmp/m14-proyectos/Vagrant/prova1$ ls
Vagrantfile

a201037jf@i06:/var/tmp/m14-proyectos/Vagrant/prova1$ vagrant up

a201037jf@i06:~/VirtualBox VMs$ nmap localhost 
Starting Nmap 7.93 ( https://nmap.org ) at 2024-02-21 12:52 CET
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000068s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 995 closed tcp ports (conn-refused)
PORT     STATE SERVICE
22/tcp   open  ssh
25/tcp   open  smtp
111/tcp  open  rpcbind
631/tcp  open  ipp
2222/tcp open  EtherNetIP-1


```
Hay que hacerlo todo en el directorio de conexto (dodne esta el VagrantFile)
```
a201037jf@i06:/var/tmp/m14-proyectos/Vagrant/prova1$ vagrant status
Current machine states:

default                   running (virtualbox)

The VM is running. To stop this VM, you can run `vagrant halt` to
shut it down forcefully, or you can run `vagrant suspend` to simply
suspend the virtual machine. In either case, to restart it again,
simply run `vagrant up`.

$ vagrant ssh


# Con esto también tendremos el fichero en el directorio de contexto del host
vagrant@vagrant:~$ ls > /vagrant/kk


```


## Directorio vagrant
Aquí esta la definición del vagrant, podemos ver hasta la clave privada que usamos automaticamente cuando hacemos "vagrant ssh", también lo podríamos hacer:
$ ssh -i .vagrant/machines/default/virtualbox/private_key -p 2222 vagrant@localhost

```
a201037jf@i06:/var/tmp/m14-proyectos/Vagrant/prova1$ tree .vagrant/
.vagrant/
├── machines
│   └── default
│       └── virtualbox
│           ├── action_provision
│           ├── action_set_name
│           ├── box_meta
│           ├── creator_uid
│           ├── id
│           ├── index_uuid
│           ├── private_key
│           ├── synced_folders
│           └── vagrant_cwd
└── rgloader
    └── loader.rb

```

# Prueba alpine

Cuando hago el "vagrant up", se baja el el alpine, crea una clave compartida, propaga el puerto 2222 por defecto:

```
 1907  mkdir alpine
 1908  cd alpine/
 1909  vagrant init generic/alpine38 --minimal
 1910  cat Vagrantfile 
 1911  vagrant up
 1912  vagrant ssh

``` 

## Las "imagenes" en vagrant se llaman box
$ vagrant box list
generic/alpine38   (virtualbox, 4.3.12, (amd64))
hashicorp/bionic64 (virtualbox, 1.0.282)

$ vagrant box add generic/fedora32
==> box: Loading metadata for box 'generic/fedora32'
    box: URL: https://vagrantcloud.com/api/v2/vagrant/generic/fedora32
This box can work with multiple providers! The providers that it
can work with are listed below. Please review the list and choose
the provider you will be working with.

1) hyperv
2) libvirt
3) parallels
4) qemu
5) virtualbox
6) vmware_desktop

Enter your choice: 5
==> box: Adding box 'generic/fedora32' (v4.3.12) for provider: virtualbox (amd64)
    box: Downloading: https://vagrantcloud.com/generic/boxes/fedora32/versions/4.3.12/providers/virtualbox/amd64/vagrant.box
    box: Calculating and comparing box checksum...
==> box: Successfully added box 'generic/fedora32' (v4.3.12) for 'virtualbox (amd64)'!

$ vagrant box remove generic/fedora32
Removing box 'generic/fedora32' (v4.3.12) with provider 'virtualbox'...

## Ejemplo de "provisioning"
He creado la estructura normal, pero añadiendo el bootstrap.sh e index.html

```
a201037jf@i06:/var/tmp/m14-proyectos/Vagrant/prova1$ tree -a
.
├── bootstrap.sh
├── index.html
├── kk
├── .vagrant
│   ├── machines
│   │   └── default
│   │       └── virtualbox
│   │           ├── action_provision
│   │           ├── action_set_name
│   │           ├── box_meta
│   │           ├── creator_uid
│   │           ├── id
│   │           ├── index_uuid
│   │           ├── private_key
│   │           ├── synced_folders
│   │           └── vagrant_cwd
│   └── rgloader
│       └── loader.rb
└── Vagrantfile
```
