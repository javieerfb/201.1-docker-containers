# Repte 2 Provisioning de README, git i docker
## Enunciado redumido:
En aquest repte s’estudiaran algunes dels elements de personalització en la creació
d’imatges automatitzades per a màquines virtuals. En especial:
● Sync de directoris
● Còpia de fitxers
● Provisioning amb shell inline i amb path.

## Práctica

Empezamos con una imagen generic/alpine38:
```
$ vagrant init --minimal generic/alpine38
```

Vamos por partes según lo que nos pide:

1. Sincronitzar el directori de desenvolupament d’aquesta pràctica del host a dins de la imatge Vagrant a la ubicació /vagrant_data.

(Pág 33 del HowTo)

Hay que editar el Vagrantfile

Basta con esta línea que sincroniza el directorio activo (de contexto) del host, con el directorio /vagrant_data del guest que será la máquina vagrant.

```
  config.vm.synced_folder ".", "/vagrant_data", create: true
```

2. Crear un fitxer README.md descriptiu de l’escola, el cicle, el mòdul i aquesta pràctica. Copiar-lo a la ubicació /var/tmp usant el provision de tipus file.

Creo el readme y lo copio con la línea siguiente en el Vagrantfile
Importante que el directorio de destino termine en '/', en mi caso me falló por eso.

También hacer el "vagrant reload --provision"

O simplmente hacer "vagrant provision"

```
config.vm.provision "file", source: "./README.md", destination: "/var/tmp/"
```

3. Usant el provision shell inline mostrar l’execució de l’ordre “uname -a”.

Por ejemplo:
```
  config.vm.provision "shell",
    inline: "uname -a"
```

Al iniciar los provisioners, nos mostrará el resultado por pantalla:
```
$ vagrant provision
...
==> default: Running provisioner: shell...
    default: Running: inline script
    default: Linux alpine38.localdomain 4.14.167-0-virt #1-Alpine SMP Thu Jan 23 10:58:18 UTC 2020 x86_64 Linux
...

```

4. Usant el provision shell inline instal·lar Docker (per alpine), configurar l’usuari perquè pertanyi al grup docker i activar el servei. Si es tracta de múltiples línies d’instruccions escriure un shel inline amb multilinies i un marcador de final.

Primero hay que hacer un vagrant ssh e ir probando que el proceso funciona, en mi caso ya lo he probado, y ahora puedo poner la serie de ordenes en el vagrantfile

```
$install_docker = <<-FIN
sudo apk add docker
sudo addgroup $(whoami) docker
sudo rc-update add docker default
sudo service docker start
FIN

...

 config.vm.provision "shell", inline: $install_docker
end
```

Luego, hay que hacer un reinicio aplicando el provisioning
$ vagrant reload --provision

5. Usant un provision shell path executar usant un fitxer script la instal·lació de git, la creació dels directori /usr/share/doc/edtasix i dins seu la descàrrega dels repositoris git (un git clone) dels apunts M06-ASO i de M14-projectes.

Añadimos:
```
  config.vm.provision "shell", path: "git-install.sh"
```

El script es el siguiente:
```
sudo apk add git
sudo mkdir -p /usr/share/doc/edtasix
git clone https://gitlab.com/edtasixm14/m14-projectes.git /usr/share/doc/edtasix/m14
git clone https://gitlab.com/edtasixm06/m06-aso.git /usr/share/doc/edtasix/m06

```

6. Generar la máquina virtual, basta con hacer un "vagrant reload --provision".
Para tenerla con lo necesario.

7. Creo una cuenta en vagrant cloud
Voy a seguir esta guia porque me hago un poco de lio:

https://blog.ycshao.com/2017/09/16/how-to-upload-vagrant-box-to-vagrant-cloud/

Hay que empaquetar la box con:
**$ vagrant package --output repte2.box**
Para poder especificar luego la carpeta comprimida como "provisioning"

Cuando ya esta released (hay que darle especificamente a un botón que diga "release version", no es automático), podremos usarla fijandonos en la sección "How to use this box with Vagrant:".

En "New" nos pone como iniciarla, hay que hacer un dir nuevo, y hacerlo para probar que ha funcionado.

```
vagrant init javieerfb/edt24base \
  --box-version 1.0.0
vagrant up
```

8. EXTRA: Actualizar la versión
Si quisiera cambiar algo de la versión que tengo subida al Vagrant Cloud, debería de volver a generar la imagen una vez editada, y volver a empaquetarla y luego subirla.

