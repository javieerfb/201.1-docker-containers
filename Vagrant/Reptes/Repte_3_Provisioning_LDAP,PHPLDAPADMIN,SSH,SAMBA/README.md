# Repte 3 Provisioning de serveis: ldap, phpldapadmin, ssh i samba

Enunciado:
En aquest repte s’ampliarà l’exercici del repte anterior desplegant els serveis docker
fets a l’assignatura de M06 de LDAP, phpLdapAdmin, SSH i SAMBA. Tots ells lligats
amb els usuaris de LDAP i preferentment usant un desplegament amb docker
compose.

Usar una imatge de tipus generic/alpine38.

Descarregar en la imatge localment les imatges docker necessàries, copiar-hi els
fitxers necessaris per al desplegament dels serveis en docker i automatitzar que
s'engeguin en engegar la màquina virtual.

## Practica

1. Empezando por lo básico, hay que tener tanto docker, como docker-compose.
He hecho un script básico para tenerlos listos, luego he añadido el provisioning
```
  config.vm.provision "shell", path: "docker-install.sh"
```

2. Habrá que tener las máquinas listas
