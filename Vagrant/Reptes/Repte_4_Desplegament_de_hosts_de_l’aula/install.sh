echo 'Buenos dias'
# No interactivo
export DEBIAN_FRONTEND=noninteractive
sudo apt-get install -y tree krb5-user krb5-multidev libpam-mount sssd nfs-common autofs ufw curl

# Ficheros
 git clone https://gitlab.com/manelmellado/ubnt-at-inf.git
 cd ubnt-at-inf/Debian\ 11/arxius/etc/
 sudo cp sssd/sssd.conf /etc/sssd/
 sudo cp sudoers.d/inf /etc/sudoers.d/
 sudo chmod 600 /etc/sssd/sssd.conf

 sudo pam-auth-update --force --enable mkhomdir


