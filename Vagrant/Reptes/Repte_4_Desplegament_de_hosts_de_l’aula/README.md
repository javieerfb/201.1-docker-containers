# Repte 4 Desplegament de hosts de l’aula
En aquest repte es crearà la configuració per a desplegar amb Vagrant màquines
virtuals Debian configurades com si es tractessin de hosts de l’aula.

Caldrà que utilitzin una interfície de xarxa tipus bridge per poder ser hosts
independents que rebin l’adreça IP de l’aula.

Caldrà que automàticament disposin de la post instal·lació (totalment automatitzada)
per configurar el host com un més de l’aula, permetent l’autenticació dels alumnes i
professors.

Aquest repte utilitzarà un Box Debian-11 (debian/bullseye64).
Pujar la imatge al Vagrant Cloud (al compte personal) amb el nom usuari/edt22aula.

# Practica
1. Imagen
$ vagrant init generic/debian11 --minimal

2. Preparar el provision y el networking (modo bridge)
```
  config.vm.network "public_network", bridge: "eno1"
  config.vm.provision "shell", path: "install.sh"
```

3. Seguiremos el proceso de instalación de:
[install-at-inf - Debian11](https://gitlab.com/edtasixm14/m14-projectes/-/tree/master/install-at-inf/Debian_11)


4.

