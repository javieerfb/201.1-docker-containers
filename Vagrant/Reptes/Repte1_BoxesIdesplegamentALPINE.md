# Repte 1 Gestió d’imatges (Box) i desplegament d’un alpine

## Configurar els providers
No puedo porque no tengo sudo, pero da igual.

## Gestionar les imatges (Box)
### Afegir els següents Box (practicar afegir, llistar i eliminar)
+ Afegir dos box de alpine generic/alpine38, un per libvirtd i un per virtualbox
```
$ vagrant box add generic/alpine38 --provider libvirt
$ vagrant box add generic/alpine38 --provider virtualbox
```

Aquí podemos ver el tema de que los boxes pueden compartir nombre siempre y cuando tengan un provider diferente.
```
$ vagrant box list
generic/alpine38   (libvirt, 4.3.12, (amd64))
generic/alpine38   (virtualbox, 4.3.12, (amd64))
```

+ Afegir dos box debian bullseye64, també per a libvirtd i virtualbox.
```
$ vagrant box add debian/bullseye64 --provider virtualbox
$ vagrant box add debian/bullseye64 --provider libvirt
```

+ Un box de ubuntu per virtualbox.
```
$ vagrant box add ubuntu/focal64 --provider virtualbox
```

+ Un box per fedora cloud 37 per a libvirtd.
```
$ vagrant box add fedora/37-cloud-base --provider libvirt
```

### Vagrant Cloud
> ● Navegar per vagrant cloud i identificar les imatges que hi ha, en especial les
de Debian, Fedora i Ubuntu. Observar si són oficials o no.
Según el propio Vagrant, es el nombre de usuario que crea la box el que debería de verificar la autenticidad de la misma:
https://developer.hashicorp.com/vagrant/vagrant-cloud/boxes/catalog



> ● Observar de les imatges per a quins providers n’existeixen versions.
Por ejemplo, ubuntu solo hace versiones para Virtualbox.
```
The box you're attempting to add doesn't support the provider
you requested.
```


