# Ejercicio 1: Virtualización

Tendré una máquina virtual Debian 12.

## Características
Dicha máquina tendrá estas características:
+ 50 GB (Habrá que hacer un RAID poco después de empezar el proyecto, mínimo que tenga capacidad neta de 10GB)
+ Nombre equipo (impuesto por la práctica de UOC: ARSO20231
+ El usuario debe ser el mismo que tengo en el instituto (doy por hecho)

## Paquetes necesarios
+ Host y Máquina Virtual
	+ nmap
	+ curl
+ Máquina Virtual
	+ git
	+ docker + docker compose

## RAID
EL RAID que voy a implementar es el RAID 0.

El escenario que propone la pŕactica lo lógico sería el RAID 1 para obtener la tolerancia a fallos. Resumidamente conseguiría tener 2 discos con los mismo datos.

El motivo por el que me he decidico por el RAID 0 es el de aprovechar mejor el espacio del que dispongo y mejorar la velocidad de lectura / escritura, aún sabiendo que esto conlleva el riesgo de no ser tolerante a fallos, y que si uno de los 2 discos falla los datos se perderán.

## Evidencias

