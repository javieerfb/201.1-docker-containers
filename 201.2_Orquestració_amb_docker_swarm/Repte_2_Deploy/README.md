# Ennciado:
> Apunts: HowTo-ASIX-Docker-Cloud-Machine-Swarm.pdf capítol “Get Started with Swarm”.
> Implementar l’exemple “Get started with Swarm”

https://gitlab.com/edtasixm05/docker/-/blob/master/getstarted/swarm/getstarted_swarm_part.pdf

Para hacer el deploy hay que estar dentro de un swarm

## Ejercicio
### Redis + web

```
$ docker swarm init
$ docker swarm leave --force
$ docker swarm init
Swarm initialized: current node (4qxuq2tmz571n37094i7tozfg) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-1x5ckm3lqq5qzh8rlzce31p0v0180lp4wx56dys4103u8wrny1-bb6yiv7cya9skdofatrt512w0 10.200.243.206:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

```

Para que un trabajador se pueda unir al swarm, debe autenticarse presentando el token que se menciona.

El puerto lo ha abierto el propio docker swarm para que se puedan comunicar entre maquinas

$ docker stack deploy -c compose.yml myapp      #

a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker stack ps myapp
ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
kbel4ybhc9np   myapp_redis.1        redis:latest                      i06       Running         Running 40 seconds ago             
vq6qfx7we91t   myapp_visualizer.1   dockersamples/visualizer:stable   i06       Running         Running 39 seconds ago             
3shjh8d8dunu   myapp_web.1          edtasixm05/getstarted:comptador   i06       Running         Running 36 seconds ago             
l17rbuwr8dn0   myapp_web.2          edtasixm05/getstarted:comptador   i06       Running         Running 36 seconds ago  


Si ahora comprobamos el localhost:80, tendremos 2 máquinas con el mismo puerto propagado.
Eso no sería posible de no ser por estar en swarm.
De esta forma al comprobarlo, vemos que hay 2 hostnames diferentes que se van intercalando por cada refresh.
Esto es porque hacen un balanceo de carga !



### Visualizer
http://localhost:8080/

### Más hostnames mayor balanceo de carga
Editado el compose.yml
    deploy:
      replicas: 5



a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker stack deploy -c compose.yml myapp    #Le pedimos que el estado acctial sea el deseado
Updating service myapp_web (id: prr4fst1aroq50fqsvlzwoj2u)
Updating service myapp_redis (id: i8wbia5q20af9w85twyp0v0yl)
Updating service myapp_visualizer (id: igvaj7vl68el6tpflkohp5cis)

a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker stack ps myapp
ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
kbel4ybhc9np   myapp_redis.1        redis:latest                      i06       Running         Running 9 minutes ago              
vq6qfx7we91t   myapp_visualizer.1   dockersamples/visualizer:stable   i06       Running         Running 9 minutes ago              
3shjh8d8dunu   myapp_web.1          edtasixm05/getstarted:comptador   i06       Running         Running 9 minutes ago              
l17rbuwr8dn0   myapp_web.2          edtasixm05/ge
tstarted:comptador   i06       Running         Running 9 minutes ago              
cdiyynwgzawk   myapp_web.3          edtasixm05/getstarted:comptador   i06       Running         Running 32 seconds ago             
5jpswmmzs0ld   myapp_web.4          edtasixm05/getstarted:comptador   i06       Running         Running 33 seconds ago             
y3zi0auxwovd   myapp_web.5          edtasixm05/getstarted:comptador   i06       Running         Running 32 seconds ago             


## Docker Service
```
a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker service ls
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
i8wbia5q20af   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
igvaj7vl68el   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
prr4fst1aroq   myapp_web          replicated   5/5        edtasixm05/getstarted:comptador   *:80->80/tcp

a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker service ps myapp_redis
ID             NAME            IMAGE          NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
kbel4ybhc9np   myapp_redis.1   redis:latest   i06       Running         Running 13 minutes ago             
a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker service ps myapp_web
ID             NAME          IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
3shjh8d8dunu   myapp_web.1   edtasixm05/getstarted:comptador   i06       Running         Running 13 minutes ago             
l17rbuwr8dn0   myapp_web.2   edtasixm05/getstarted:comptador   i06       Running         Running 13 minutes ago             
cdiyynwgzawk   myapp_web.3   edtasixm05/getstarted:comptador   i06       Running         Running 3 minutes ago              
5jpswmmzs0ld   myapp_web.4   edtasixm05/getstarted:comptador   i06       Running         Running 4 minutes ago              
y3zi0auxwovd   myapp_web.5   edtasixm05/getstarted:comptador   i06       Running         Running 3 minutes ago              
a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker service logs myapp_visualizer
myapp_visualizer.1.vq6qfx7we91t@i06    | npm info it worked if it ends with ok
myapp_visualizer.1.vq6qfx7we91t@i06    | npm info using npm@5.3.0
myapp_visualizer.1.vq6qfx7we91t@i06    | npm info using node@v8.2.1
myapp_visualizer.1.vq6qfx7we91t@i06    | npm info lifecycle swarmVisualizer@0.0.1~prestart: swarmVisualizer@0.0.1
myapp_visualizer.1.vq6qfx7we91t@i06    | npm info lifecycle swarmVisualizer@0.0.1~start: swarmVisualizer@0.0.1
myapp_visualizer.1.vq6qfx7we91t@i06    | 
myapp_visualizer.1.vq6qfx7we91t@i06    | > swarmVisualizer@0.0.1 start /app
myapp_visualizer.1.vq6qfx7we91t@i06    | > node server.js
myapp_visualizer.1.vq6qfx7we91t@i06    | 
myapp_visualizer.1.vq6qfx7we91t@i06    | undefined

a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker service scale myapp_web=2   #Para que no haya 5 myweb services
myapp_web scaled to 2
overall progress: 2 out of 2 tasks 
1/2: running   [==================================================>] 
2/2: running   [==================================================>] 
verify: Service converged 
a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker service ps myapp_web
ID             NAME          IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
3shjh8d8dunu   myapp_web.1   edtasixm05/getstarted:comptador   i06       Running         Running 15 minutes ago             
l17rbuwr8dn0   myapp_web.2   edtasixm05/getstarted:comptador   i06       Running         Running 15 minutes ago             
cdiyynwgzawk   myapp_web.3   edtasixm05/getstarted:comptador   i06       Remove          Running 11 seconds ago             
5jpswmmzs0ld   myapp_web.4   edtasixm05/getstarted:comptador   i06       Remove          Running 11 seconds ago             
a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker stack rm myapp
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet
y3zi0auxwovd   myapp_web.5   edtasixm05/getstarted:comptador   i06       Remove          Running 11 seconds ago           
a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
4qxuq2tmz571n37094i7tozfg *   i06        Ready     Active         Leader           24.0.2
a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker swarm leave -f  #Salir del node
Node left the swarm.

```

**(!)Convergir significa que el conjunto de elementos adopta el estado deseado**


## Practica de clase
Apuntes: https://gitlab.com/edtasixm05/docker/-/tree/master/3_docker_intermig/swarm?ref_type=heads

La practica se ejemplifica aquí:
https://gitlab.com/edtasixm05/docker/-/blob/master/3_docker_intermig/swarm/play-with-docker.md?ref_type=heads



En un grupo de 3 por ejemplo.

1 hace de líder, los otros 2 hacen de "trabajadores"

```
a201037jf@i06:/tmp/docker/3_docker_intermig/deploy$ docker swarm init
Swarm initialized: current node (21owloaukutot1ujw5v4jbv3n) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-3eqqvam9lr2hskuwa9fycu5koruek0l8clb0q8zluy1quwsjll-6a33cqqswdjdzdz40lxgwuow9 10.200.243.206:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

```

Con esto

Luego el líder hace:
$docker stack deploy -c compose.yml prog

Los trabajadores no tienen (ni pueden) hacer nada, automaticamente se les despliega el compose en el equipo.

Si el líder lo mira con el visualizer, se podrá ver en horizontal los distintos hosts (a modo de columna)

Luego en vertical a modo de filas (como en el anterior), se ven los servicios iniciados.






