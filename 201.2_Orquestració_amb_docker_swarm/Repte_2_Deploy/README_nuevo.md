# Repte2-Deploy

Apuntes: 
https://gitlab.com/edtasixm05/docker/-/tree/master/3_docker_intermig/swarm?ref_type=heads

## Iniciar swarm

### Manager y worker en el mismo swarm

**En el MANAGER**
```
 docker swarm init
Swarm initialized: current node (r80fvrkq9woz3f0ql6uk7hs20) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-18oe5rfdvow9y605ghz8rre5n6kvt3l2y415kzvq3xd0kgn05e-9lk8egx04qa54e5ruo8vb3ddx 10.200.243.206:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

**En el otro host:**

```
a201037jf@i05:~$ docker swarm join --token SWMTKN-1-18oe5rfdvow9y605ghz8rre5n6kvt3l2y415kzvq3xd0kgn05e-9lk8egx04qa54e5ruo8vb3ddx 10.200.243.206:2377
This node joined a swarm as a worker.
```
### Desplegar servicios
1. Despliegue
```
$ docker stack deploy -c compose.yml  myapp
Creating network myapp_webnet
Creating service myapp_redis
Creating service myapp_visualizer
Creating service myapp_web
```
2. Comprobar

```
$ docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
8ms2qprxm76h   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
xlrmz0xpzs6d   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
9ph8qj48zukn   myapp_web          replicated   5/5        edtasixm05/getstarted:comptador   *:80->80/tcp


$ docker stack ps myapp
ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
n07z929dqjmp   myapp_redis.1        redis:latest                      i06       Running         Running 55 seconds ago             
jd3gw9xpk5qg   myapp_visualizer.1   dockersamples/visualizer:stable   i06       Running         Running 53 seconds ago             
wpncor42oatj   myapp_web.1          edtasixm05/getstarted:comptador   i05       Running         Running 45 seconds ago             
nch1yc8j5rc8   myapp_web.2          edtasixm05/getstarted:comptador   i06       Running         Running 50 seconds ago             
itvbpe9tcip5   myapp_web.3          edtasixm05/getstarted:comptador   i05       Running         Running 45 seconds ago             
nknirkjr1gix   myapp_web.4          edtasixm05/getstarted:comptador   i06       Running         Running 50 seconds ago             
ia84b5juhx5a   myapp_web.5          edtasixm05/getstarted:comptador   i05       Running         Running 45 seconds ago 
```

### Cambiar el número de replicas: scale
```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_2_Deploy$ docker service scale myapp_web=3
myapp_web scaled to 3
overall progress: 3 out of 3 tasks 
1/3: running   [==================================================>] 
2/3: running   [==================================================>] 
3/3: running   [==================================================>] 
verify: Service converged 
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_2_Deploy$ docker stack ps myapp
ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
n07z929dqjmp   myapp_redis.1        redis:latest                      i06       Running         Running 3 minutes ago             
jd3gw9xpk5qg   myapp_visualizer.1   dockersamples/visualizer:stable   i06       Running         Running 3 minutes ago             
wpncor42oatj   myapp_web.1          edtasixm05/getstarted:comptador   i05       Running         Running 3 minutes ago             
nch1yc8j5rc8   myapp_web.2          edtasixm05/getstarted:comptador   i06       Running         Running 3 minutes ago             
itvbpe9tcip5   myapp_web.3          edtasixm05/getstarted:comptador   i05       Running         Running 3 minutes ago             
nknirkjr1gix   myapp_web.4          edtasixm05/getstarted:comptador   i06       Remove          Running 7 seconds ago             
ia84b5juhx5a   myapp_web.5          edtasixm05/getstarted:comptador   i05       Remove          Running 7 seconds ago  
```

### Visualizer:
En el navegador:
http://localhost:8080/

### Plegar:
Eliminar el stack desplegado:
```
$ docker stack rm myapp
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet
```

Salir del swarm (En los 2 equipos):

```
$ docker swarm leave --force
Node left the swarm.

```
