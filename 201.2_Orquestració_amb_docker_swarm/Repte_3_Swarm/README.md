# Repte 3 Swarm
Apuntes:
https://gitlab.com/edtasixm05/docker/-/tree/master/3_docker_intermig/swarm?ref_type=heads

## docker swarm

1. Iniciar el swarm
```
To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-538rt8as42p6qita5ejl22mw1ioyf9gtb7an74nm7vu2tqaxgu-1e7buhkqheoz6cxubzzn09a7f 10.200.243.206:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

2. Que el worker se una
```
docker swarm join --token SWMTKN-1-538rt8as42p6qita5ejl22mw1ioyf9gtb7an74nm7vu2tqaxgu-1e7buhkqheoz6cxubzzn09a7f 10.200.243.206:2377
a201037jf@i05:~$ docker swarm join --token SWMTKN-1-538rt8as42p6qita5ejl22mw1ioyf9gtb7an74nm7vu2tqaxgu-1e7buhkqheoz6cxubzzn09a7f 10.200.243.206:2377
This node joined a swarm as a worker.
```

3. Ver los nodos del swarm (manager)
```
 docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
kh9yqkkkazo65x9a29yvfc4c9     i05        Ready     Active                          24.0.2
sw9ur2j8fbsbicjvbaygxx7ly *   i06        Ready     Active         Leader           24.0.2
```

4. Cerrar swarm (ambos nodos)
```
a201037jf@i05:~$ docker swarm leave
Node left the swarm.

a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_3_Swarm$ docker swarm leave -f
Node left the swarm.
```

## docker nodes (Esta parte la hago con el PWD)

### PWD play-with-docker (SSH)
En el PWD, maquina de manager:
$ cat .ssh/id_rsa
1. Y copio el contenido en un fichero local, que luego cuando haga el ssh, pondré la opción -i, y especificaré el nombre del fichero con la clave privada del manager.
2. a201037jf@i06:/tmp$ vim id_rsa_manager
3. a201037jf@i06:/tmp$ chmod 600 id_rsa_manager 
4. a201037jf@i06:/tmp$ ssh -i id_rsa_manager ip172-18-0-125-cli8t0ssnmng00d4f54g@direct.labs.play-with-docker.com



1. Listar los nodos activos

