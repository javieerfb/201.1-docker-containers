# Debian11 con systemd y docker
Esta imagen sirve para probar cosas de docker, principalmente swarm.
Tiene instalado:
+ systemd con PID 1
+ docker y docker compose con sus repositorios y certificados
+ nmap, y demás
+ Puertos expuestos:
    + 2377
    + 7946
    + 4789

Es importante destacar, que en realidad la exposición de los puertos se aplica en el compose.
Tal que así:
```
  service-name:
    
    ...
    
    ports:
      - "2377:2377"
      - "7946:7946"
      - "7946:7946/udp"
      - "4789:4789/udp"
```

## Para ejecutar esto (Individual de prueba):
```
docker build -t javierfb/debian11:systemdYdocker .
docker run --rm -d --privileged --name debian11-systemdYdocker javierfb/debian11:systemdYdocker
docker exec -it debian11-systemdYdocker /bin/bash
```

## Para ejecutar el docker compose:
```
docker compose up -d
docker exec -it manager|worker1|worker2 /bin/bash
```
