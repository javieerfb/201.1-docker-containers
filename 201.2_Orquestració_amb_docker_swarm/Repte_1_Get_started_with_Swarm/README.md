# Geting Started with swarm
Los apuntes: https://gitlab.com/edtasixm05/docker/-/blob/master/getstarted/swarm/getstarted_swarm_part.pdf?ref_type=heads

ENUNCIADO:
> Repte 1 Get started with Swarm
> Apunts: HowTo-ASIX-Docker-Cloud-Machine-Swarm.pdf capítol “Get Started with Swarm”.
> Implementar l’exemple “Get started with Swarm”


## Parte 1: SWARM

### Creación y puesta a punto con el Manager
Se crea el swarm, 

**MANAGER (i06)**

1. Se crea el swarm
```
$ docker swarm init --advertise-addr $(hostname -i)
Swarm initialized: current node (qmlrjs7exi754r76sz2o69pd0) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-4z7fkceesue5pohi11nj9n4lprb46b37yt2butqvdxan8fki6q-1f9j54y9mjp5ql83hzetemr7p 10.200.243.206:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

2. Se generan los tokens (claves) para unirse como manager y worker, luego con esos tokens los usaremos para que cada equipo tenga un rol concreto
```
$ docker swarm join-token manager
To add a manager to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-4z7fkceesue5pohi11nj9n4lprb46b37yt2butqvdxan8fki6q-al8205czraofwtp3sa6oms38m 10.200.243.206:2377

$ docker swarm join-token worker
To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-4z7fkceesue5pohi11nj9n4lprb46b37yt2butqvdxan8fki6q-1f9j54y9mjp5ql83hzetemr7p 10.200.243.206:2377
```

### Conexión del worker
**WORKER (i05)**
1. Me uno con el rol de worker (el i05):
```
a201037jf@i05:~$ docker swarm join --token SWMTKN-1-4z7fkceesue5pohi11nj9n4lprb46b37yt2butqvdxan8fki6q-1f9j54y9mjp5ql83hzetemr7p 10.200.243.206:2377
This node joined a swarm as a worker.

```

### Pruebas desplegando servicios y demás
**MANAGER (volvemos)**

1. Desplegamos un servicio, en el ejemplo, es un servicio llamado "helloworld", la imagen usada es un alpine, y ejecuta al iniciar un "ping a docker.com"

```
$ docker service create --replicas 1 --name helloworld alpine ping docker.com
d0zy2u5dphoxg2kdbj5wnxzc1
overall progress: 1 out of 1 tasks 
1/1: running   [==================================================>] 
verify: Service converged 

$ docker service ls
ID             NAME         MODE         REPLICAS   IMAGE           PORTS
d0zy2u5dphox   helloworld   replicated   1/1        alpine:latest   
```

2. Inspeccionamos el servicio:
Con el:
*docker service inspect --pretty <nombre_puesto_al_servicio>*


```
$ docker service ps helloworld
ID             NAME           IMAGE           NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
ohvo1tcbuz7c   helloworld.1   alpine:latest   i06       Running         Running 5 minutes ago        

$ docker service inspect --pretty helloworld | tail -5
 Args:		ping docker.com 
 Init:		false
Resources:
Endpoint Mode:	vip

```

3. Aumentamos la escala
En lugar de iniciar 1 solo servicio en el swarm, iniciaremos 5 replicas

Esto se hace con un servicio que ya se está ejecutando, simplemente lo replica, pero teniendo ya la base.
```
$ docker service scale <nombre del servicio>=<Nombre de replicas>

$ docker service scale helloworld=5
helloworld scaled to 5
overall progress: 5 out of 5 tasks 
1/5: running   [==================================================>] 
2/5: running   [==================================================>] 
3/5: running   [==================================================>] 
4/5: running   [==================================================>] 
5/5: running   [==================================================>] 
verify: Service converged 

```

4. Comprobamos:
Ahora tenemos 5 servicios funcionando, pero al estar en swarm, entre los nodos (equipos que están en el mismo swarm), se comparten la carga de trabajo. En este caso 3 servicios están en el worker, y 2 en el manager.


(!) Este al ser el manager, puede ver el servicio con mas detalle, el worker no puede modificar ni ver el estado de los servicios
**MANAGER**
```

a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker service ps helloworld
ID             NAME           IMAGE           NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
ohvo1tcbuz7c   helloworld.1   alpine:latest   i06       Running         Running 15 minutes ago             
8vpwwx8bpsam   helloworld.2   alpine:latest   i05       Running         Running 5 minutes ago              
l13slkbcn35c   helloworld.3   alpine:latest   i05       Running         Running 5 minutes ago              
wn3rixxqj42j   helloworld.4   alpine:latest   i05       Running         Running 5 minutes ago              
g7z7yce6wbgq   helloworld.5   alpine:latest   i06       Running         Running 5 minutes ago        


a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker ps
CONTAINER ID   IMAGE           COMMAND             CREATED              STATUS              PORTS     NAMES
6127cad0039f   alpine:latest   "ping docker.com"   About a minute ago   Up About a minute             helloworld.5.g7z7yce6wbgq619nwpfbemns5
11156ad047ed   alpine:latest   "ping docker.com"   11 minutes ago       Up 11 minutes                 helloworld.1.ohvo1tcbuz7cjcoqkysj95rzb

```

**WORKER**
```
a201037jf@i05:~$ docker service ps helloworld
Error response from daemon: This node is not a swarm manager. Worker nodes can't be used to view or modify cluster state. Please run this command on a manager node or promote the current node to a manager.


a201037jf@i05:~$ docker ps
CONTAINER ID   IMAGE           COMMAND             CREATED         STATUS         PORTS     NAMES
63aa7b93a569   alpine:latest   "ping docker.com"   2 minutes ago   Up 2 minutes             helloworld.4.wn3rixxqj42jfoih5qkw2bwka
40d0062fd8d2   alpine:latest   "ping docker.com"   2 minutes ago   Up 2 minutes             helloworld.2.8vpwwx8bpsami6obhtn0aw09h
ee00e305e6af   alpine:latest   "ping docker.com"   2 minutes ago   Up 2 minutes             helloworld.3.l13slkbcn35ca55ooojlo52yt

```

5. Eliminamos el servicio

Al hacer el rm, tarda unos segundos, así que mejor no hacer el ps al instante de hacerlo.

```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker service rm helloworld
helloworld
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker service ls
ID        NAME      MODE      REPLICAS   IMAGE     PORTS
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES


```

## Parte REDIS
1. Se inician 3 servicios de redis:
Lo que hace es desplegar 3 replicas de la imagen redis:3.0.6, cada 10 segundos comprueba que se haya actualizado en el despliegue.

**MANAGER**
```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker service ps redis
ID             NAME      IMAGE         NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
jw6uswre46w6   redis.1   redis:3.0.6   i06       Running         Running 38 seconds ago             
9scns88d8d9n   redis.2   redis:3.0.6   i05       Running         Running 38 seconds ago             
5bv9s09wgna4   redis.3   redis:3.0.6   i05       Running         Running 38 seconds ago             
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED          STATUS          PORTS      NAMES
1c59def15591   redis:3.0.6   "/entrypoint.sh redi…"   46 seconds ago   Up 45 seconds   6379/tcp   redis.1.jw6uswre46w6d5v038q093kdl
```

**WORKER**
```
a201037jf@i05:~$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED          STATUS          PORTS      NAMES
f7899f3c10fd   redis:3.0.6   "/entrypoint.sh redi…"   51 seconds ago   Up 50 seconds   6379/tcp   redis.3.5bv9s09wgna4b5vgv7zmm7yvd
51dc3c6340d0   redis:3.0.6   "/entrypoint.sh redi…"   51 seconds ago   Up 50 seconds   6379/tcp   redis.2.9scns88d8d9nbib2tifuvbg8n

```

2. Actualizar la imagen de redis usada en el despliegue, y comprobar que automaticamente se aplica el cambio
El comando actualiza la imagen de redis del servicio "redis"

```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker service update --image redis:3.0.7 redis
redis
overall progress: 3 out of 3 tasks 
1/3: running   [==================================================>] 
2/3: running   [==================================================>] 
3/3: running   [==================================================>] 
verify: Service converged 
```



**MANAGER**
```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker service inspect --pretty redis | grep Image
 Image:		redis:3.0.7@sha256:730b765df9fe96af414da64a2b67f3a5f70b8fd13a31e5096fee4807ed802e20

a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker service ps redis
ID             NAME          IMAGE         NODE      DESIRED STATE   CURRENT STATE                 ERROR     PORTS
az5foq59etls   redis.1       redis:3.0.7   i06       Running         Running 48 seconds ago                  
jw6uswre46w6    \_ redis.1   redis:3.0.6   i06       Shutdown        Shutdown 49 seconds ago                 
nfa13iux4vk5   redis.2       redis:3.0.7   i05       Running         Running about a minute ago              
9scns88d8d9n    \_ redis.2   redis:3.0.6   i05       Shutdown        Shutdown about a minute ago             
sgffuluzqoge   redis.3       redis:3.0.7   i06       Running         Running about a minute ago              
5bv9s09wgna4    \_ redis.3   redis:3.0.6   i05       Shutdown        Shutdown about a minute ago             

a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED              STATUS              PORTS      NAMES
7ef2cc990f87   redis:3.0.7   "docker-entrypoint.s…"   About a minute ago   Up About a minute   6379/tcp   redis.1.az5foq59etlswcs2ivbihefwm
f87b4f6eaaad   redis:3.0.7   "docker-entrypoint.s…"   About a minute ago   Up About a minute   6379/tcp   redis.3.sgffuluzqoge2p4niq11kml9f

```
(!) Lo curioso es que haciendo el docker service ps redis, también me dice los que se han apagado (como si hiciera el docker ps -a)


**WORKER**
```
a201037jf@i05:~$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED              STATUS              PORTS      NAMES
adbbeb5af003   redis:3.0.7   "docker-entrypoint.s…"   About a minute ago   Up About a minute   6379/tcp   redis.2.nfa13iux4vk54ul5i5vu8fzk1

```

3. Eliminar el servicio
```
$ docker service rm redis
redis
```

## Gestíon de los nodos
Lo que hago es vovler a desplegar 3 replicas de redis, al estar en el swarm, se comparte la carga.
Pero al actualizar la disponibilidad del nodo worker (i05), este deja de aceptar los servicios, así que toda la carga pasa al manager en este caso.

**MANAGER**
1. Despliegue y comprobar servicios que hay

```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker service create --replicas 3 --name redis --update-delay 10s redis:3.0.6
fof8yvi2fquyc4z0dr4lsl3gh
overall progress: 3 out of 3 tasks 
1/3: running   [==================================================>] 
2/3: running   [==================================================>] 
3/3: running   [==================================================>] 
verify: Service converged 
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED          STATUS         PORTS      NAMES
e70504c64611   redis:3.0.6   "/entrypoint.sh redi…"   10 seconds ago   Up 9 seconds   6379/tcp   redis.2.c0fh777h6m3a4768z80hotv6v

```

**WORKER**
```
a201037jf@i05:~$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED          STATUS          PORTS      NAMES
c65414799515   redis:3.0.6   "/entrypoint.sh redi…"   17 seconds ago   Up 15 seconds   6379/tcp   redis.1.vggmlx8fzl4uzs75ehfs5nbtl
1f578af6d444   redis:3.0.6   "/entrypoint.sh redi…"   17 seconds ago   Up 15 seconds   6379/tcp   redis.3.56jxp7e1zcdm85t695znuu5jt

```

2. Actualizar disponibilidad del nodo que es un worker
```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker node update --availability drain i05
i05
```

3. Resultado; El worker deja de aceptar los servicios ejecutandose en su equipo (por parte del swarm), y el Manager pasa a tenerlos.
Esto sucede porque ya que el worker ya no acepta las tareas del swarm, las que estaban activas, pasan a otro nodo, y al ser el manager el único que queda, púes se queda con todas las tareas para el.

**MANAGER**
```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED          STATUS                  PORTS      NAMES
6bf58d64b134   redis:3.0.6   "/entrypoint.sh redi…"   2 seconds ago    Up Less than a second   6379/tcp   redis.3.44x3pjxnst8mut7tltbx00239
47c4eaaa1b76   redis:3.0.6   "/entrypoint.sh redi…"   2 seconds ago    Up Less than a second   6379/tcp   redis.1.6ows7p0fy0uyctf0ivg2r92wo
e70504c64611   redis:3.0.6   "/entrypoint.sh redi…"   39 seconds ago   Up 38 seconds           6379/tcp   redis.2.c0fh777h6m3a4768z80hotv6v

```

**WORKER**
```
a201037jf@i05:~$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES


```

4. (Observación)
Si vuelvo a actualizar la disponibilidad del worker de vuelta, este no aceptará las tareas que tiene el manager (3), pero si las nuevas.
Si hago un scale, para que el total de replicas sean 5 (2 más de las que tengo ahora), si que se hace el reparto.

**MANAGER**
```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker node update --availability active i05
i05

a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker service scale redis=5
redis scaled to 5
overall progress: 5 out of 5 tasks 
1/5: running   [==================================================>] 
2/5: running   [==================================================>] 
3/5: running   [==================================================>] 
4/5: running   [==================================================>] 
5/5: running   [==================================================>] 
verify: Service converged 

a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED          STATUS          PORTS      NAMES
6bf58d64b134   redis:3.0.6   "/entrypoint.sh redi…"   10 minutes ago   Up 10 minutes   6379/tcp   redis.3.44x3pjxnst8mut7tltbx00239
47c4eaaa1b76   redis:3.0.6   "/entrypoint.sh redi…"   10 minutes ago   Up 10 minutes   6379/tcp   redis.1.6ows7p0fy0uyctf0ivg2r92wo
e70504c64611   redis:3.0.6   "/entrypoint.sh redi…"   11 minutes ago   Up 11 minutes   6379/tcp   redis.2.c0fh777h6m3a4768z80hotv6v

```

**WORKER**
```
a201037jf@i05:~$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED          STATUS          PORTS      NAMES
cc8f3a24f2b4   redis:3.0.6   "/entrypoint.sh redi…"   14 seconds ago   Up 13 seconds   6379/tcp   redis.4.imfaszh8o4efjd0si7gwuasnn
d489ff627e7f   redis:3.0.6   "/entrypoint.sh redi…"   14 seconds ago   Up 13 seconds   6379/tcp   redis.5.ncbuujo4eprwdhocpcq8d949g

```

## Parte de "swarm mode routing mesh", no voy a traducirlo

1. Desplegamos 2 replicas del servicio "my-web", que ejecuta el servicio nginx(web)
```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker service create \
--name my-web \
--publish published=8080,target=80 \
--replicas 2 \
nginx
u5oeb4uv5rmxirefpagkit0lc
overall progress: 2 out of 2 tasks 
1/2: running   [==================================================>] 
2/2: running   [==================================================>] 
verify: Service converged
```

2. Publicamos el puerto 80 del container al 8080, (ya se han definido al desplegar el servico)
```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker service update \
--publish-add published=8080,target=80 \
my-web
my-web
overall progress: 2 out of 2 tasks 
1/2: running   [==================================================>] 
2/2: running   [==================================================>] 
verify: Service converged 
```

3. Importante, incluso haciendo que 1 de los nodos deje de ejecutar el servicio, podremos seguir accediendo a este con la ip del nodo, en este cado el worker:

```
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker node update --availability drain i05
i05
a201037jf@i06:/var/tmp/m14-proyectos/201.2_Orquestració_amb_docker_swarm/Repte_1_Get_started_with_Swarm$ docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS          PORTS     NAMES
aff4b0e86bb4   nginx:latest   "/docker-entrypoint.…"   39 seconds ago   Up 35 seconds   80/tcp    my-web.1.043excci6h4fnihre6bpp6yqj
4c0911c26292   nginx:latest   "/docker-entrypoint.…"   6 minutes ago    Up 6 minutes    80/tcp    my-web.2.lyuol9qo3mrcz3yh0hgyj2lbe
```


