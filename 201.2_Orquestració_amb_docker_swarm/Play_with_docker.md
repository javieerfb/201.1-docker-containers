# Usar el playground de docker
Apuntes: 
https://gitlab.com/edtasixm05/docker/-/blob/master/3_docker_intermig/swarm/play-with-docker.md?ref_type=heads

Es como usar una máquina virtual, pero tiene docker!

Si hacemos clic en el icono de llave inglesa, podemos desplegar ráidamente 1 manager y 1 worker

Ahora si hago "$ docker node ls" en el manager:
```
[manager1] (local) root@192.168.0.8 ~
$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
2i4ctg5u01eduzxqfrpvx67e0 *   manager1   Ready     Active         Leader           24.0.7
momalx5qty1ifffb0bj0fx60r     worker1    Ready     Active                          24.0.7
```

## Practica 1
Creo una nueva instancia, y la uniré al swarm.

Para eso en el docker manager, obtendremos el token:
```
$ docker swarm join-token worker
To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-1k01lcnhlkj250i5j1xt1voucf6hye76fxqd1duydokk8h5sv5-0u65udtryo5tdih32h2ljc064 192.168.0.8:2377
```

### Para hacer el ssh en el worker2:
worker2:
```
[node1] (local) root@192.168.0.6 ~
$ cat .ssh/id_rsa
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACBlxyi89vBZk8iISR70zEF1KMq8S/Xe3OebPQJyMNFd0QAAAJgZ05i7GdOY
uwAAAAtzc2gtZWQyNTUxOQAAACBlxyi89vBZk8iISR70zEF1KMq8S/Xe3OebPQJyMNFd0Q
AAAECbQoApAGpgiRSLjGiOSmbN80A0/9vJW2T+BNjVNPkLjGXHKLz28FmTyIhJHvTMQXUo
yrxL9d7c55s9AnIw0V3RAAAAFHJvb3RAYnVpbGRraXRzYW5kYm94AQ==
-----END OPENSSH PRIVATE KEY-----
```

En local:
Copia la clave privada en un fichero local, que usare para iniciar el ssh con opción -i
Cambio permisos, y me conecto con la comanda ssh que he copiado de la instancia

```
a201037jf@i06:/tmp$ ssh -i id_rsa ip172-18-0-169-cli7uoogftqg00c864g0@direct.labs.play-with-docker.com
Connecting to 20.231.1.80:8022
###############################################################
#                          WARNING!!!!                        #
# This is a sandbox environment. Using personal credentials   #
# is HIGHLY! discouraged. Any consequences of doing so are    #
# completely the user's responsibilites.                      #
#                                                             #
# The PWD team.                                               #
###############################################################

[node1] (local) root@192.168.0.6 ~
$ docker swarm join --token SWMTKN-1-1k01lcnhlkj250i5j1xt1voucf6hye76fxqd1duydokk8h5sv5-0u65udtryo5tdih32h2ljc064 192.168.0.8:2377
This node joined a swarm as a worker.

```

Haré lo mismo con el manager

docker stack deploy -c compose.yml myapp


Hemos practicado la sección:
https://gitlab.com/edtasixm05/docker/-/tree/master/3_docker_intermig/swarm?ref_type=heads

> Colocació de recursos als nodes: Labels & Placement &  Constraints
> Y demás


$ docker stack rm myapp
$ docker swarm leave
